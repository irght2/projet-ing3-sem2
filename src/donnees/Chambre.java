package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Chambre {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	private int no_chambre;
	private int surveillant;
	private int nb_lits;
	
	/**cle secondaire de service (a voir car pas d'idee pour l'instant*/
	String code_service;
	
	
	/**getter No Chambre
	 * @return int No Chambre*/
	public int getNo_Chambre() {
		return this.no_chambre;
	}
	
	/**getter Surveillant
	 * @return int Surveillant*/
	public int surveillant() {
		return this.surveillant;
	}
	
	/**getter Nb lits
	 * @return int Nb lits*/
	public int getNb_lits_chambre() {
		return this.nb_lits;
	}
	
	
	/**setter No Chambre
	 * @param numero Numero de chambre*/
	public void setNo_Chambre(int numero) {
		this.no_chambre=numero;
		
	}
	
	/**setter Surveillant
	 * @param new_surveillant Nouveau surveillant*/
	public void setSurveillant(int new_surveillant) {
		this.surveillant=new_surveillant;
		
	}
	
	/**setter Nb lits
	 * @param nombre Nombre de lits*/
	public void setNb_lits_chambre(int nombre) {
		this.nb_lits=nombre;
	}
	
	
	/**Add query*/
	/**insertion de la chambre avec verifications necessaires
	 * @param param1 Parametre 1
	 * @param param2 Parametre 2
	 * @param param3 Parametre 3
	 * @param param4 Parametre 4
	 * @param objetConnexion Objet de la classe Connexion
	 * @return String Exceptions*/
	public String insertionChambre(String param1, int param2, int param3, int param4,Connexion objetConnexion) {
		
		String exception=null;
		
		/**on verifie que le surveillant specifie existe (si il est present dans la table infirmiere)*/
		
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		/**verification si code service existe*/
		ArrayList<Vector<String>> verification2 = new ArrayList<Vector<String>>();
		String [][] enregistre2 = null;
		
		/**verification si code service existe*/
		ArrayList<Vector<String>> verification3 = new ArrayList<Vector<String>>();
		String [][] enregistre3 = null;
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT numero FROM infirmier WHERE numero="+param3);
			enregistre = new String[verification.size()][1];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					System.out.println("tableau 1 : "+enregistre[i][j]);
					
				}
			}
			
			//verification2=objetConnexion.remplirChampsRequete("SELECT * FROM service WHERE code= 'CAR'"/*+param2*/);
			verification2=objetConnexion.remplirChampsRequete("SELECT * FROM service WHERE code='"+param1+"'");
			enregistre2=new String[verification2.size()][4];
			
			for (int i=0;i<verification2.size();i++) {
				for(int j=0;j<verification2.get(i).size();j++) {
					
					enregistre2[i][j]=verification2.get(i).get(j).toString();
					System.out.println("tableau 2 :"+enregistre2[i][j]);
					
				}
			}
			
			verification3=objetConnexion.remplirChampsRequete("SELECT code_service FROM infirmier WHERE code_service='"+param1+"' AND numero="+param3);
			if(verification3.size()!=0) {
				//System.out.println("Erreur dans le sous programme insertion chambre ! ");
				
				enregistre3=new String[verification2.size()][1];
				
				for (int i=0;i<verification3.size();i++) {
					for(int j=0;j<verification3.get(i).size();j++) {
						
						enregistre3[i][j]=verification3.get(i).get(j).toString();
						System.out.println("tableau 3 :"+enregistre3[i][j]);
						
					}
				}
				
				
			}
			
		
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero ou du service saisi pour l'insertion d'une chambre");
		}
		
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur, impossible d'inserer la chambre, le numero du surveillant specifie est introuvable";
			System.out.println("erreur tableau 1");
		}
		
		else if(verification2.size()==0) {
			
			exception="Erreur, le service precise n'existe pas";
			System.out.println("erreur tableau 2");
			
		}
		
		else if(verification3.size()==0) {
			
			exception="Erreur, le surveillant specifie ne fait pas partie du service specifie";
		}
		
		
		else {
			
			try {
				objetConnexion.executeUpdate("INSERT INTO chambre VALUES ('"+param1+"',"+param2+","+param3+","+param4+")");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				// TODO Auto-generated catch block
				
				exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
				
			} catch(SQLException e1) {
				
				exception="Erreur, nombre, type ou valeur de parametre incorrect !";
				//e1.printStackTrace();
			}
			
			
			
		}
		return exception;
		
	}
	
	/**recherche d'un chambre
	 * @param userChoiceBox UserChoiceBox
	 * @param userTextSearch UserTextSearch
	 * @param connectObject Objet de la classe Connexion
	 * @return ArrayList Resultat de requete*/
	public ArrayList<Vector<String>> rechercheChambre(JCheckBox userChoiceBox, JTextField userTextSearch, Connexion connectObject) {
		
		ArrayList<Vector<String>> tableauChambre = new ArrayList<Vector<String>>();

		try {
			//tableauMalade=connectObject.remplirChampsRequete("SELECT * FROM malade WHERE "+a+"=6");
			tableauChambre=connectObject.remplirChampsRequete("SELECT * FROM chambre WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauChambre;
	}
		
	public void deleteChambre(String numero,Connexion objetConnexion) {
		
		try {
			objetConnexion.executeUpdate("DELETE FROM chambre where no_chambre = '"+numero+"'");
			System.out.println("Ca marche");
		} catch (MySQLIntegrityConstraintViolationException e) {
			// TODO Auto-generated catch block
			
		} catch(SQLException e1) {
			
		}
	
	}
	
	/** search query
	 * @param param1 Parametre 1
	 * @param param2 Parametre 2
	 * @param param3 Parametre 3
	 * @param param4 Parametre 4
	 * @param objetConnexion Objet de la classe Connexion*/
	public void searchChambre(int param1,String param2,int param3, String param4,Connexion objetConnexion ) {
		
		try {
                ArrayList<Vector<String>> test;
                test=new ArrayList<>();
                
                test=objetConnexion.remplirChampsRequete("SELECT * FROM  ");
                System.out.println(test);
                
             } catch (SQLException e1) {
             // TODO Auto-generated catch block
                 e1.printStackTrace();
              }
                
                
            }
	
	/** update query
	 * @param numero Numero de la chambre
	 * @param surveillant Surveillant de la chambre
	 * @param lit Nombre de lits
	 * @param objetConnexion Objet de la classe Connexion*/
	public void updateChambre(String numero, String surveillant, String lit, Connexion objetConnexion ) {
		
		ArrayList<Vector<String>> test = null;
		
		try {
			test=objetConnexion.remplirChampsRequete("SELECT no_chambre FROM chambre WHERE no_chambre='"+numero+"'");
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String essai;
				
			essai=	test.get(0).get(0).toString();
		
		System.out.println(essai);
		
		
		if(essai.equals(numero)) {
			
			try {
				
				if(surveillant.equals(null)||surveillant.equals("")) {
					objetConnexion.executeUpdate("UPDATE chambre SET nb_lits='"+lit+"' WHERE no_chambre='"+numero+"'");
				}
				else if(lit.equals(null)||lit.equals("")) {
					objetConnexion.executeUpdate("UPDATE chambre SET surveillant='"+surveillant+"' WHERE no_chambre='"+numero+"'");
				}
				else {
					objetConnexion.executeUpdate("UPDATE chambre SET nb_lits='"+lit+"', surveillant='"+surveillant+"' WHERE no_chambre='"+numero+"'");
				}
				
			}catch (MySQLIntegrityConstraintViolationException e) {
				// TODO Auto-generated catch block
				
			} catch(SQLException e1) {
				
			}
		}else {
			javax.swing.JOptionPane.showMessageDialog(null,"Cette chambre n'existe pas");
		}
			
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
}
