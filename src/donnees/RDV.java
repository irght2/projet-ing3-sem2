package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class RDV {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	private int heure_RDV;
	private String date_RDV;
	/**Cle secondaire*/
	private String code_service;
	/**Cle secondaire*/
	private int no_employe;
	private int no_malade;
	
	/**getters*/
	public int getHeure_RDV() {
		return this.heure_RDV;
	}
	
	public String getDate_RDV() {
		return this.date_RDV;
	}
	
	/**setters*/
	public void setHeure_RDV(int h) {
		this.heure_RDV=h;
	}
	
	public void setDate_RDV(String d) {
		this.date_RDV=d;
	}
	/**DELETE QUERY */
	public void deleteRDV(String date,String heure,String service, String employe, String malade,Connexion objetConnexion) {
		
		if((!"".equals(date))&&(!"".equals(heure))&&(!"".equals(service))&&(!"".equals(employe))&&(!"".equals(malade))) {
			
		
		
		try {
				objetConnexion.executeUpdate("DELETE FROM rdv WHERE code_service='"+service+"'AND date='"+date+"' AND heure='"+heure+"'AND no_employe='"+employe+"'AND no_malade='"+malade+"'");		
		}catch(MySQLIntegrityConstraintViolationException e) {
			
		}catch(SQLException e1) {
			
		}
		
		}else {
			javax.swing.JOptionPane.showMessageDialog(null,"Cet RDV n'existe pas");
	}
}
	
	
	
	/**insertion de l'infirmier avec toutes les verifications necessaires*/
	public String insertionRDV(String param1,String param2, int param3, int param4, int param5, Connexion objetConnexion) {

		String exception=null;
		
		
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		
		ArrayList<Vector<String>> verification2 = new ArrayList<Vector<String>>();
		String [][] enregistre2 = null;
		
	
		ArrayList<Vector<String>> verification3 = new ArrayList<Vector<String>>();
		String [][] enregistre3 = null;
		
		
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT code FROM service WHERE code='"+param2+"'");
			enregistre = new String[verification.size()][1];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					//System.out.println("tableau 1 : "+enregistre[i][j]);
					
				}
			}
			
			//verification2=objetConnexion.remplirChampsRequete("SELECT * FROM service WHERE code= 'CAR'"/*+param2*/);
			verification2=objetConnexion.remplirChampsRequete("SELECT numero FROM employes WHERE numero="+param4);
			enregistre2=new String[verification2.size()][1];
			
			for (int i=0;i<verification2.size();i++) {
				for(int j=0;j<verification2.get(i).size();j++) {
					
					enregistre2[i][j]=verification2.get(i).get(j).toString();
					System.out.println("tableau 2 :"+enregistre2[i][j]);
					
				}
			}
			
			verification3=objetConnexion.remplirChampsRequete("SELECT numero FROM malade WHERE numero="+param5);
			enregistre3=new String[verification3.size()][1];
			
			for (int i=0;i<verification3.size();i++) {
				for(int j=0;j<verification3.get(i).size();j++) {
					
					enregistre3[i][j]=verification3.get(i).get(j).toString();
					//System.out.println("tableau 2 :"+enregistre3[i][j]);
					
				}
			}


		
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero ou du service saisi pour l'insertion d'un infirmier");
		}
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur, le service specifie est inexistant";
			
		}
		
		else if(verification2.size()==0) {
			
			exception="Erreur, le numero de l'employe specifie est inexistant";
			
			
		}
		
		else if(verification3.size()==0) {
			
			exception="Erreur, le numero de malade specifie est inexistant ";
			
			
		}
		
		else {
			
			try {
				
				objetConnexion.executeUpdate("INSERT INTO rdv VALUES ('"+param1+"','"+param2+"',"+param3+","+param4+","+param5+")");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				
				// TODO Auto-generated catch block
				exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
				
			} catch(SQLException e1) {
				
				exception="Erreur, nombre, type ou valeur de parametre incorrect !";
				//e1.printStackTrace();
			}
			
		}
		
		return exception;
	}

}