package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Employe {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	private int no_employe;
	private String nom_employe,prenom_employe,tel_employe,adresse_employe;
	
	
	/**getter No Employe
	 * @return int No Employe*/
	public int getNo_employe() {
		return this.no_employe;
	}
	
	/**getter Nom Employe
	 * @return String Nom Employe*/
	public String getNom_employe() {
		return this.nom_employe;
	}
	
	/**getter Prenom Employe
	 * @return String Prenom Employe*/
	public String getPrenom_employe() {
		return this.prenom_employe;
	}
	
	/**getter Tel Employe
	 * @return String Tel Employe*/
	public String getTel_employe() {
		return this.tel_employe;
	}
	
	/**getter Adresse Employe
	 * @return String Adresse Employe*/
	public String getAdresse_employe() {
		return this.adresse_employe;
	}
	
	

	/**setter No Employe
	 * @param num Numero*/
	public void setNo_employe(int num) {
		this.no_employe=num;
	}
	
	/**setter Nom Employe
	 * @param n Nom*/
	public void setNom_employe(String n) {
		this.nom_employe=n;
	}
	
	/**setter Prenom Employe
	 * @param p Prenom*/
	public void setPrenom_employe(String p) {
		this.prenom_employe=p;
	}
	
	/**setter Tel Employe
	 * @param t Telephone*/
	public void setTel_employe(String t) {
		this.tel_employe=t;
		
	}
	
	/**setter Adresse Employe
	 * @param a Adresse*/
	public void setAdresse_employe(String a) {
		this.adresse_employe=a;
	}
	
	/**Add query*/
	/**insertion d'un employe avec verif necessaires*/
	public String insertionEmploye(int param1,String param2,String param3,String param4,String param5,Connexion objetConnexion) {

		
		String exception=null;
		
		try {
	
			objetConnexion.executeUpdate("INSERT INTO employes VALUES ("+param1+",'"+param2+"','"+param3+"','"+param4+"','"+param5+"')");
			exception="Insertion was successfully made";
			
		} catch (MySQLIntegrityConstraintViolationException e) {
			
			// TODO Auto-generated catch block
			exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
			
		} catch(SQLException e1) {
			
			exception="Erreur, nombre ou type de parametre incorrect !";
			
		}
		
		return exception;
	}

	/**Delete Query*/
	/**delete employes avec gestion des delete qui en decoulent*/
	public void deleteEmploye(String numero, String nom, String prenom, Connexion objetConnexion) {
		try {
			if (numero.equals("")||numero.equals(null)) {
				ArrayList<Vector<String>> test;
				
				test=objetConnexion.remplirChampsRequete("SELECT numero FROM employes WHERE nom='"+nom+"' AND prenom='"+prenom+"'" );
				
				String essai;
						
					essai=	test.get(0).get(0).toString();
						
						System.out.println(essai);	
				
				objetConnexion.executeUpdate("DELETE FROM employes where nom='"+nom+"' AND prenom='"+prenom+"'");
				objetConnexion.executeUpdate("DELETE FROM docteur where numero = '"+essai+"'");
				objetConnexion.executeUpdate("DELETE FROM infirmier where numero = '"+essai+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne where no_docteur = '"+essai+"'");
				
				
			}
			else if((nom.equals(null)||nom.equals(""))&&(prenom.equals(null)||prenom.equals(""))) {
				
				objetConnexion.executeUpdate("DELETE FROM employes where numero = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM docteur where numero = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM infirmier where numero = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne where no_docteur = '"+numero+"'");
			}
			else {
				
				objetConnexion.executeUpdate("DELETE FROM employes where numero = '"+numero+"'AND nom='"+nom+"'AND prenom='"+prenom+"'" );
				objetConnexion.executeUpdate("DELETE FROM docteur where numero = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM infirmier where numero = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne where no_docteur = '"+numero+"'");
			}
			
		} catch (MySQLIntegrityConstraintViolationException e) {
			// TODO Auto-generated catch block
			
		} catch(SQLException e1) {
			
		}
	}
	
	/**Update Query*/
	/**update employes*/
	public void updateEmploye(String numero, String nom, String prenom, String adresse, String tel, Connexion objetConnexion) {
		
		
		
		ArrayList<Vector<String>> test = null;
		
		try {
			test=objetConnexion.remplirChampsRequete("SELECT numero FROM employes WHERE numero='"+numero+"'");
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String essai;
				
			essai=	test.get(0).get(0).toString();
		
		System.out.println(essai);
		
		
		
	if(essai.equals(numero)) {
		try {
			
			if(nom.equals(null)||nom.equals("")) {
				
				if(prenom.equals(null)||prenom.equals("")) {
					
					if(adresse.equals(null)||adresse.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"' WHERE numero='"+numero+"'");						
					}
					else if(tel.equals(null)||tel.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
					}					
				}
				else if(adresse.equals(null)||adresse.equals("")) {
					
					if(prenom.equals(null)||prenom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"' WHERE numero='"+numero+"'");						
					}
					else if(tel.equals(null)||tel.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}				
				}
				else if(tel.equals(null)||tel.equals("")) {
					
					if(prenom.equals(null)||prenom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"' WHERE numero='"+numero+"'");						
					}
					else if(adresse.equals(null)||adresse.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}	
					
				}else {
					objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"', adresse='"+adresse+"', tel='"+tel+
							"' WHERE numero='"+numero+"'");
				}
			}
			else if(prenom.equals(null)||prenom.equals("")) {
				
				if(nom.equals(null)||nom.equals("")) {
					
					if(adresse.equals(null)||adresse.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"' WHERE numero='"+numero+"'");						
					}
					else if(tel.equals(null)||tel.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
					}					
				}
				else if(adresse.equals(null)||adresse.equals("")) {
					
					if(nom.equals(null)||nom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"' WHERE numero='"+numero+"'");						
					}
					else if(tel.equals(null)||tel.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"', nom='"+nom+"' WHERE numero='"+numero+"'");
					}				
				}
				else if(tel.equals(null)||tel.equals("")) {
					
					if(nom.equals(null)||nom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"' WHERE numero='"+numero+"'");						
					}
					else if(adresse.equals(null)||adresse.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"', nom='"+nom+"' WHERE numero='"+numero+"'");
					}	
					
				}else {
					objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"', adresse='"+adresse+"', tel='"+tel+
							"' WHERE numero='"+numero+"'");
				}
			}
			else if(adresse.equals(null)||adresse.equals("")) {
				
				if(nom.equals(null)||nom.equals("")) {
					
					if(prenom.equals(null)||prenom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"' WHERE numero='"+numero+"'");						
					}
					else if(tel.equals(null)||tel.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}					
				}
				else if(prenom.equals(null)||prenom.equals("")) {
					
					if(nom.equals(null)||nom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"' WHERE numero='"+numero+"'");						
					}
					else if(tel.equals(null)||tel.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET tel='"+tel+"', nom='"+nom+"' WHERE numero='"+numero+"'");
					}				
				}
				else if(tel.equals(null)||tel.equals("")) {
					
					if(nom.equals(null)||nom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"' WHERE numero='"+numero+"'");						
					}
					else if(prenom.equals(null)||prenom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"', nom='"+nom+"' WHERE numero='"+numero+"'");
					}	
					
				}else {
					objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"', prenom='"+prenom+"', tel='"+tel+
							"' WHERE numero='"+numero+"'");
				}	
			}
			else if(tel.equals(null)||tel.equals("")) {
				
				if(nom.equals(null)||nom.equals("")) {
					
					if(prenom.equals(null)||prenom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"' WHERE numero='"+numero+"'");						
					}
					else if(adresse.equals(null)||adresse.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
					}					
				}
				else if(prenom.equals(null)||prenom.equals("")) {
					
					if(nom.equals(null)||nom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"' WHERE numero='"+numero+"'");						
					}
					else if(adresse.equals(null)||adresse.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET adresse='"+adresse+"', nom='"+nom+"' WHERE numero='"+numero+"'");
					}				
				}
				else if(adresse.equals(null)||adresse.equals("")) {
					
					if(nom.equals(null)||nom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"' WHERE numero='"+numero+"'");						
					}
					else if(prenom.equals(null)||prenom.equals("")) {
						objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"' WHERE numero='"+numero+"'");
					}
					else {
						objetConnexion.executeUpdate("UPDATE employes SET prenom='"+prenom+"', nom='"+nom+"' WHERE numero='"+numero+"'");
					}	
					
				}else {
					objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"', prenom='"+prenom+"', adresse='"+adresse+
							"' WHERE numero='"+numero+"'");
				}
			}
			else {
				objetConnexion.executeUpdate("UPDATE employes SET nom='"+nom+"', prenom='"+prenom+"', adresse='"+adresse+
							"' , tel='"+tel+"' WHERE numero='"+numero+"'");		
			}			
			
		} catch (MySQLIntegrityConstraintViolationException e) {
			// TODO Auto-generated catch block
			
		} catch(SQLException e1) {
			
		}
	}else {
		javax.swing.JOptionPane.showMessageDialog(null,"Cet employe n'existe pas");
	}
		
	
	}
	
	/**recherche employe par nom,prenom etc.*/
	public ArrayList<Vector<String>> rechercheEmploye(JCheckBox userChoiceBox, JTextField userTextSearch, Connexion connectObject) {
		
		ArrayList<Vector<String>> tableauEmploye = new ArrayList<Vector<String>>();

		try {
			
			tableauEmploye=connectObject.remplirChampsRequete("SELECT * FROM employes WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauEmploye;
	}
	
	/**recherche employe etendue avec gestion des joitures de tables*/
	public ArrayList<Vector<String>> rechercheEmployeEtendue(ArrayList<Vector<String>> tableauRetourne, Connexion objetC){
		
		ArrayList<Vector<String>> tableauInfirmierDocteur = new ArrayList<Vector<String>>();
		
		/**on parcourt le tableau retourne*/
		String testtttttt=tableauRetourne.get(0).get(0).toString();
		System.out.println("le testtttttt retourne : "+testtttttt);
		
		try {
			tableauInfirmierDocteur=objetC.remplirChampsRequete("SELECT * FROM docteur WHERE numero='"+testtttttt+"'");
			
			if(tableauInfirmierDocteur.size()==0) {
				
				tableauInfirmierDocteur=objetC.remplirChampsRequete("SELECT * FROM infirmier WHERE numero='"+testtttttt+"'");
				System.out.println("jsdfjdshfskjdfh");
				
			}
			
			System.out.println("tableauInfimerDocteur OK");
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String [][] enregistre = null;
		enregistre = new String[tableauInfirmierDocteur.size()][5];
		
		for (int i=0;i<tableauInfirmierDocteur.size();i++) {
			for(int j=0;j<tableauInfirmierDocteur.get(i).size();j++) {
				
				//enregistre[i][j]=tableauInfirmierDocteur.get(i).get(j).toString();
				//System.out.println("tableau 1 : "+enregistre[i][j]);
				System.out.println(tableauInfirmierDocteur);
			}
		}
		System.out.println("test avant return ");
	
		return tableauInfirmierDocteur;
	}
	
	
}
	

	


