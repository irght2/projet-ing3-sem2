package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Malade {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	private int no_malade;
	private String nom_malade,prenom_malade,tel_malade,adresse_malade,mutuelle_malade;
	
	/**getters*/
	public int getNo_malade() {
		return this.no_malade;
	}
	
	public String getNom_malade() {
		return this.nom_malade;
	}
	
	public String getPrenom_malade() {
		return this.prenom_malade;
	}
	
	public String getTel_malade() {
		return this.tel_malade;
	}
	
	public String getAdresse_malalde() {
		return this.adresse_malade;
	}
	
	public String getMutuelle_malade() {
		return this.mutuelle_malade;
	}
	
	/**setters*/
	public void setNo_malade(int n) {
		this.no_malade=n;
	}
	
	public void setNom_malade(String nm) {
		this.nom_malade=nm;
	}
	
	public void setPrenom_malade(String pm) {
		this.prenom_malade=pm;
	}
	
	public void setTel_malade(String tm) {
		this.tel_malade=tm;
	}
	
	public void setAdresse_malade(String am) {
		this.adresse_malade=am;
	}
	
	public void setMutuelle_malade(String mm) {
		this.mutuelle_malade=mm;
	}
	
	
	/**Add query*/
	/**Inserer des malades dans la base de donnees (tous les champs doivent etre renseignes*/
	public String insertionMalade(int param1,String param2,String param3,String param4,String param5,String param6,Connexion objetConnexion) {
		
		String exception=null;
		
		try {
	
			objetConnexion.executeUpdate("INSERT INTO malade VALUES ("+param1+",'"+param2+"','"+param3+"','"+param4+"','"+param5+"','"+param6+"')");
			exception="Insertion was successfully made";
			
		} catch (MySQLIntegrityConstraintViolationException e) {
			
			// TODO Auto-generated catch block
			exception="Erreur, vous ne pouvez pas ins\u00e9erer d'\u00e9el\u00e9ements d'\u00e9eja pr\u00e9esents dans la base. Veuillez r\u00e9eessayer s'il vous plait !";
			
		} catch(SQLException e1) {
			
			exception="Nombre ou type de parametre incorrect, veuillez r\u00e9eessayer s'il vous plait !";
		}
		
		return exception;
	}
	
	
	/**rechercher des malades par leur nom, prenom, numero ou mutuelle*/
	public ArrayList<Vector<String>> rechercheMalade(JCheckBox userChoiceBox, JTextField userTextSearch, Connexion connectObject) {
		
		ArrayList<Vector<String>> tableauMalade = new ArrayList<Vector<String>>();

		try {
			//tableauMalade=connectObject.remplirChampsRequete("SELECT * FROM malade WHERE "+a+"=6");
			tableauMalade=connectObject.remplirChampsRequete("SELECT * FROM malade WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauMalade;
	}
	
	
	
	/**Delete Query*/
	/**Suppresion de malade*/
	public void deleteMalade(String numero, String nom, String prenom, Connexion objetConnexion) {
		System.out.println("coucoui");
		try {
			if (numero.equals("")||numero.equals(null)) {
				ArrayList<Vector<String>> test;
				
				test=objetConnexion.remplirChampsRequete("SELECT numero FROM malade WHERE nom='"+nom+"' AND prenom='"+prenom+"'" );
				
				String essai;
						
					essai=	test.get(0).get(0).toString();
						
						System.out.println(essai);	
				
				objetConnexion.executeUpdate("DELETE FROM malade where nom='"+nom+"' AND prenom='"+prenom+"'");
				objetConnexion.executeUpdate("DELETE FROM hospitalisation WHERE no_malade='"+essai+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne WHERE no_malade='"+essai+"'");
				
			}
			else if((nom.equals(null)||nom.equals(""))&&(prenom.equals(null)||prenom.equals(""))) {
				
				objetConnexion.executeUpdate("DELETE FROM malade where numero = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM hospitalisation where no_malade = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne WHERE no_malade='"+numero+"'");
				
			}
			else {
				
				objetConnexion.executeUpdate("DELETE FROM malade where numero = '"+numero+"'AND nom='"+nom+"'AND prenom='"+prenom+"'" );
				objetConnexion.executeUpdate("DELETE FROM hospitalisation where no_malade = '"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne WHERE no_malade='"+numero+"'");
				
			}
			
		} catch (MySQLIntegrityConstraintViolationException e) {
			// TODO Auto-generated catch block
			
		} catch(SQLException e1) {
			
		}
	
		System.out.println("oulou");
	}



/**Update Query*/
	/**update data malades avec gestion des champs (tous les champs ne doivent pas forcement etre rempli)*/
public void updateMalade(String numero, String nom, String prenom, String adresse, String tel, String mutuelle, Connexion objetConnexion) {	
	
	ArrayList<Vector<String>> test = null;
	
	try {
		test=objetConnexion.remplirChampsRequete("SELECT numero FROM malade WHERE numero='"+numero+"'");
	} catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	String essai;
			
		essai=	test.get(0).get(0).toString();
	
	System.out.println(essai);
	
	if(essai.equals(numero)) {
		try {
		
				if(nom.equals("")||nom.equals(null)) {
			
					if(prenom.equals("")||prenom.equals(null)) {
						
						if(adresse.equals("")||adresse.equals(null)) {
							
							if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					
					
					
					
					
					
					else if(adresse.equals("")||adresse.equals(null)) {
						
						if(prenom.equals("")||prenom.equals(null)) {
							
							if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
					}
					
					
					
					
					
					else if(tel.equals("")||tel.equals(null)) {
						
						if(prenom.equals("")||prenom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+
									"' , adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					else if(mutuelle.equals("")||mutuelle.equals(null)) {
						
						if(prenom.equals("")||prenom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+
									"' , adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
						}
						
					}
					else {
							objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
					}			
					
					
					
					
					
				}
				else if(prenom.equals("")||prenom.equals(null)) {
					
					if(nom.equals("")||nom.equals(null)) {
						
						if(adresse.equals("")||adresse.equals(null)) {
							
							if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					
					
					
					
					
					
					else if(adresse.equals("")||adresse.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
					}
					
					
					
					
					
					else if(tel.equals("")||tel.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					else if(mutuelle.equals("")||mutuelle.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
						}
						
					}
					else {
							objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
					}
					
					
			
				}
				else if(adresse.equals("")||adresse.equals(null)) {
					
					if(nom.equals("")||nom.equals(null)) {
						
						if(prenom.equals("")||prenom.equals(null)) {
							
							if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					
					
					
					
					
					
					else if(prenom.equals("")||prenom.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
					}
					
					
					
					
					
					else if(tel.equals("")||tel.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(prenom.equals("")||prenom.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					else if(mutuelle.equals("")||mutuelle.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(prenom.equals("")||prenom.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
						}
						
					}
					else {
							objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+
									"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
					}
			
				}
				else if(tel.equals("")||tel.equals(null)) {
					
					if(nom.equals("")||nom.equals(null)) {
						
						if(prenom.equals("")||prenom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+
									"' , adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					
					
					
					
					
					
					else if(prenom.equals("")||prenom.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
					}
					
					
					
					
					
					else if(adresse.equals("")||adresse.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(prenom.equals("")||prenom.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
							else if(mutuelle.equals("")||mutuelle.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
							}
						}
						else if(mutuelle.equals("")||mutuelle.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , prenom='"+prenom+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
						}
						
					}
					else if(mutuelle.equals("")||mutuelle.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else if(prenom.equals("")||prenom.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
						}
						
					}
					else {
							objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+
									"' , adresse='"+adresse+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");
					}
			
				}
				else if(mutuelle.equals("")||mutuelle.equals(null)) {
					
					if(nom.equals("")||nom.equals(null)) {
						
						if(prenom.equals("")||prenom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+
									"' , adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
						}
						
					}
					
					
					
					
					
					
					else if(prenom.equals("")||prenom.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
						}
					}
					
					
					
					
					
					else if(adresse.equals("")||adresse.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(prenom.equals("")||prenom.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET tel='"+tel+"' WHERE numero='"+numero+"'");
							}
							else if(tel.equals("")||tel.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
							}
						}
						else if(tel.equals("")||tel.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , prenom='"+prenom+"', tel='"+tel+"' WHERE numero='"+numero+"'");
						}
						
					}
					else if(tel.equals("")||tel.equals(null)) {
						
						if(nom.equals("")||nom.equals(null)) {
							
							if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else if(prenom.equals("")||prenom.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
							else if(adresse.equals("")||adresse.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
							}
						}
						else if(adresse.equals("")||adresse.equals(null)) {
							
							if(nom.equals("")||nom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
							else if(prenom.equals("")||prenom.equals(null)) {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"' WHERE numero='"+numero+"'");
							}
							else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"' WHERE numero='"+numero+"'");
							}
						}
						else {
								objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+
									"' , prenom='"+prenom+"', adresse='"+adresse+"' WHERE numero='"+numero+"'");
						}
						
					}
					else {
							objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+
									"' , adresse='"+adresse+"', tel='"+tel+"' WHERE numero='"+numero+"'");
					}
			
				}
			
				
				else {
					objetConnexion.executeUpdate("UPDATE malade SET nom='"+nom+"', prenom='"+prenom+"', adresse='"+adresse+
							"' , tel='"+tel+"', mutuelle='"+mutuelle+"' WHERE numero='"+numero+"'");	
				}
			} catch (MySQLIntegrityConstraintViolationException e) {
				// TODO Auto-generated catch block
				
			} catch(SQLException e1) {
				
			}
	}
	else {
		javax.swing.JOptionPane.showMessageDialog(null,"Ce malade n'existe pas");
	}
			
		
		
	}
	
}


