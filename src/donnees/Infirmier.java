package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Infirmier extends Employe {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	/**Cle secondaire de service*/
	//private String code_service;
	//private int numero;
	private int rotation;
	private double salaire;
	
	
	/**getters*/
	public int getRotation() {
		return this.rotation;
	}
	
	public double getSalaire_infirmier() {
		return this.salaire;
	}
	
	/**setters*/
	public void setRotation(int r) {
		this.rotation=r;
	}
	
	public void setSalaire_infirmier(double s) {
		this.salaire=s;
	}
	
	/**insertion de l'infirmier avec toutes les verifications necessaires*/
	public String insertionInfirmier(int param1,String param2, String param3, int param4, Connexion objetConnexion) {

		String exception=null;
		
		/**On verifie que le numero saisi par l'utilisateur correspond a un employe*/
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		/**verification code service*/
		ArrayList<Vector<String>> verification2 = new ArrayList<Vector<String>>();
		String [][] enregistre2 = null;
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT * FROM employes WHERE numero="+param1);
			enregistre = new String[verification.size()][5];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					System.out.println("tableau 1 : "+enregistre[i][j]);
					
				}
			}
			
			//verification2=objetConnexion.remplirChampsRequete("SELECT * FROM service WHERE code= 'CAR'"/*+param2*/);
			verification2=objetConnexion.remplirChampsRequete("SELECT * FROM service WHERE code='"+param2+"'");
			enregistre2=new String[verification2.size()][4];
			
			for (int i=0;i<verification2.size();i++) {
				for(int j=0;j<verification2.get(i).size();j++) {
					
					enregistre2[i][j]=verification2.get(i).get(j).toString();
					System.out.println("tableau 2 :"+enregistre2[i][j]);
					
				}
			}

		
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero ou du service saisi pour l'insertion d'un infirmier");
		}
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur, impossible d'inserer un(e) infimier(e) qui ne fait pas partie des employes";
			System.out.println("erreur tableau 1");
		}
		
		else if(verification2.size()==0) {
			
			exception="Erreur, le service precise n'existe pas";
			System.out.println("erreur tableau 2");
			
		}
		
		else {
			
			try {
				
				objetConnexion.executeUpdate("INSERT INTO infirmier VALUES ("+param1+",'"+param2+"','"+param3+"',"+param4+")");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				
				// TODO Auto-generated catch block
				exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
				
			} catch(SQLException e1) {
				
				exception="Erreur, nombre, type ou valeur de parametre incorrect !";
				//e1.printStackTrace();
			}
			
		}
		
		return exception;
	}
		
	/**update de l'infirmier*/
	public void updateInfirmier(String numero, String rotation, String salaire, Connexion objetConnexion) {
		
		ArrayList<Vector<String>> test = null;
		
		try {
			test=objetConnexion.remplirChampsRequete("SELECT numero FROM infirmier WHERE numero='"+numero+"'");
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String essai;
				
			essai=	test.get(0).get(0).toString();
			
			
		
		System.out.println(essai);
		
		if(essai.equals(numero)) {
			
			try {	
					
					if(rotation.equals(null)||rotation.equals("")) {
					
							objetConnexion.executeUpdate("UPDATE infirmier SET salaire='"+salaire+"' WHERE numero='"+numero+"'");
							
					}
					else if(salaire.equals(null)||salaire.equals("")) {
						
							objetConnexion.executeUpdate("UPDATE infirmier SET rotation='"+rotation+"' WHERE numero='"+numero+"'");
						
					}
					else {
						objetConnexion.executeUpdate("UPDATE infirmier SET rotation='"+rotation+"', salaire='"+salaire+"' WHERE numero='"+numero+"'");
						
					}
				
				
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				// TODO Auto-generated catch block
				
			} catch(SQLException e1) {
				javax.swing.JOptionPane.showMessageDialog(null,"Merci de rentrer des valeurs coherentes");
			}
			
		}else {
			javax.swing.JOptionPane.showMessageDialog(null,"Cet infirmier n'existe pas");
		}
		
	}
	
	/**recherche infirmier avec gestion des jointures de table*/
	public ArrayList<Vector<String>> rechercheInfirmier(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauInfirmier = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier2 = new ArrayList<Vector<String>>();
		
		
		/**on recupere les infos sur l'infirmier*/
		try {
			
			tableauInfirmier =C.remplirChampsRequete("SELECT * FROM infirmier WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");	
			//System.out.println(tableauInfirmier);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			if(tableauInfirmier.size()!=0) tableauInfirmier2=C.remplirChampsRequete("SELECT infirmier.numero,employes.nom,employes.prenom,infirmier.code_service,infirmier.rotation,infirmier.salaire FROM infirmier,employes WHERE infirmier.numero="+tableauInfirmier.get(0).get(0)+" AND employes.numero="+tableauInfirmier.get(0).get(0));
				
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		return tableauInfirmier2;
	}
	
	/**rechercher infirmier avec le code service */
	public ArrayList<Vector<String>> rechercheInfirmier2(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauInfirmier = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier2 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier3 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier4 = new ArrayList<Vector<String>>();
		
		
		/**on recupere les infos sur l'infirmier*/
		try {
			
			tableauInfirmier =C.remplirChampsRequete("SELECT * FROM infirmier WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");	
			//System.out.println(tableauInfirmier);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauInfirmier.size()!=0) tableauInfirmier2=C.remplirChampsRequete("SELECT infirmier.numero FROM infirmier WHERE infirmier.code_service='"+tableauInfirmier.get(0).get(1)+"'");
			System.out.println("voici tableau I numero 2 :"+tableauInfirmier2);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauInfirmier2.size()!=0) {
				
				for(int i=0; i<tableauInfirmier2.size(); i++)
				{
					for(int j=0; j<tableauInfirmier2.get(i).size(); j++)
					{
						System.out.println(tableauInfirmier2.get(i).get(j));	
						
						tableauInfirmier3=C.remplirChampsRequete("SELECT infirmier.numero,employes.nom,employes.prenom,infirmier.code_service,infirmier.rotation,infirmier.salaire FROM infirmier,employes WHERE infirmier.code_service='"+userTextSearch.getText()+"' AND infirmier.numero="+tableauInfirmier2.get(i).get(j)+" AND employes.numero="+tableauInfirmier2.get(i).get(j));
						tableauInfirmier4.add(tableauInfirmier3.get(0));
						
					}
				}
			}
				
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		return tableauInfirmier4;
			
	}
	
	
	
	/**rechercher infirmier avec la rotation */
	public ArrayList<Vector<String>> rechercheInfirmier3(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauInfirmier = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier2 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier3 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauInfirmier4 = new ArrayList<Vector<String>>();
		
		
		/**on recupere les infos sur l'infirmier*/
		try {
			
			tableauInfirmier =C.remplirChampsRequete("SELECT * FROM infirmier WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");	
			//System.out.println(tableauInfirmier);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauInfirmier.size()!=0) tableauInfirmier2=C.remplirChampsRequete("SELECT infirmier.numero FROM infirmier WHERE infirmier.rotation='"+tableauInfirmier.get(0).get(2)+"'");
			System.out.println("voici tableau I numero 2 :"+tableauInfirmier2);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauInfirmier2.size()!=0) {
				
				for(int i=0; i<tableauInfirmier2.size(); i++)
				{
					for(int j=0; j<tableauInfirmier2.get(i).size(); j++)
					{
						System.out.println(tableauInfirmier2.get(i).get(j));	
						
						tableauInfirmier3=C.remplirChampsRequete("SELECT infirmier.numero,employes.nom,employes.prenom,infirmier.code_service,infirmier.rotation,infirmier.salaire FROM infirmier,employes WHERE infirmier.rotation='"+userTextSearch.getText()+"' AND infirmier.numero="+tableauInfirmier2.get(i).get(j)+" AND employes.numero="+tableauInfirmier2.get(i).get(j));
						tableauInfirmier4.add(tableauInfirmier3.get(0));
						
					}
				}
			}
				
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		return tableauInfirmier4;
			
	}
	
}	

