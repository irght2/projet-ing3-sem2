package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Docteur extends Employe {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	/**Herite du numero employe*/
	
	
	private String specialite;
	//private int numero_docteur;
	
	/**getter de la specialite
	 * @return String la specialite*/
	public String getSpecialite() {
		return specialite;
		
	}
	
	/**setter de la specialite
	 * @param new_specialite Nouvelle specialite*/
	public void setSpecialite(String new_specialite) {
		this.specialite=new_specialite;
		
	}
	
	/**insertion d'un docteur avec verif necessaires
	 * @param param1 Parametre 1
	 * @param param2 Parametre 2
	 * @param objetConnexion Objet de la classe Connexion
	 * @return String exception*/
	public String insertionDocteur(int param1,String param2,Connexion objetConnexion) {

		String exception=null;
		
		/**On verifie que le numero saisi par l'utilisateur correspond a un employe*/
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT * FROM employes WHERE numero="+param1);
			enregistre = new String[verification.size()][5];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					System.out.println(enregistre[i][j]);
					
				}
			}
			
		
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero saisi pour l'insertion d'un docteur");
		}
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur vous ne pouvez inserer un docteur qui ne fait pas partie des employes";
		}
		
		else {
			
			try {
				
				objetConnexion.executeUpdate("INSERT INTO docteur VALUES ("+param1+",'"+param2+"')");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				
				// TODO Auto-generated catch block
				exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
				
			} catch(SQLException e1) {
				
				exception="Erreur, nombre, type ou valeur de parametre incorrect !";
				//e1.printStackTrace();
			}
			
		}
		
		return exception;
	}
	
	/**recherche docteur avec le nom
	 * @param userChoiceBox UserChoiceBox
	 * @param userTextSearch UserTextSearch
	 * @param C Objet Connexion
	 * @return ArrayList Resultat de recherche*/
	public ArrayList<Vector<String>> rechercheDocteur(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauDocteur = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauDocteur2 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauDocteur3 = new ArrayList<Vector<String>>();
		
		
		/**on recupere les infos sur le docteur*/
		try {
			
			tableauDocteur =C.remplirChampsRequete("SELECT * FROM employes WHERE nom='"+userTextSearch.getText()+"'");	
			//System.out.println(tableauInfirmier);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauDocteur.size()!=0) tableauDocteur2=C.remplirChampsRequete("SELECT docteur.numero FROM docteur WHERE docteur.numero="+tableauDocteur.get(0).get(0));
			System.out.println("voici tableau I numero 2 :"+tableauDocteur2);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauDocteur2.size()!=0) tableauDocteur3=C.remplirChampsRequete("SELECT docteur.numero, employes.nom,employes.prenom, docteur.specialite FROM docteur,employes WHERE employes.numero="+tableauDocteur2.get(0).get(0)+" AND docteur.numero="+tableauDocteur2.get(0).get(0));
					
	
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		return tableauDocteur3;
			
	}
	
	
	/**recherche docteur avec le numero
	 * @param userChoiceBox UserChoiceBox
	 * @param userTextSearch UserTextSearch
	 * @param C Objet de Connexion
	 * @return ArrayList Resultat de recherche*/
	public ArrayList<Vector<String>> rechercheDocteur2(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauDocteur = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauDocteur2 = new ArrayList<Vector<String>>();
		//ArrayList<Vector<String>> tableauDocteur3 = new ArrayList<Vector<String>>();
		//ArrayList<Vector<String>> tableauDocteur4 = new ArrayList<Vector<String>>();
		
		
		/**on recupere les infos sur le docteur*/
		try {
			
			tableauDocteur =C.remplirChampsRequete("SELECT numero FROM employes WHERE numero='"+userTextSearch.getText()+"'");	
			//System.out.println(tableauInfirmier);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauDocteur.size()!=0) tableauDocteur2=C.remplirChampsRequete("SELECT docteur.numero, employes.nom,employes.prenom,docteur.specialite FROM docteur,employes WHERE docteur.numero="+tableauDocteur.get(0).get(0)+" AND employes.numero="+tableauDocteur.get(0).get(0));
			System.out.println("voici tableau I numero 2 :"+tableauDocteur2);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		return tableauDocteur2;
			
	}
	
	
	/**recherche docteur avec la specialite
	 * @param userChoiceBox UserChoiceBox
	 * @param userTextSearch UserTextSearch
	 * @param C Objet de Connexion
	 * @return ArrayList Resultat de recherche*/
	public ArrayList<Vector<String>> rechercheDocteur3(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauDocteur = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauDocteur2 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauDocteur3 = new ArrayList<Vector<String>>();
		
		
		/**on recupere les infos sur le docteur*/
		try {
			
			tableauDocteur =C.remplirChampsRequete("SELECT numero FROM docteur WHERE specialite='"+userTextSearch.getText()+"'");	
			//System.out.println(tableauInfirmier);
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		try {
			
			if(tableauDocteur.size()!=0) {
				
				for(int i=0; i<tableauDocteur.size(); i++)
				{
					for(int j=0; j<tableauDocteur.get(i).size(); j++)
					{
						System.out.println(tableauDocteur.get(i).get(j));	
						
						tableauDocteur2=C.remplirChampsRequete("SELECT docteur.numero, employes.nom,employes.prenom,docteur.specialite FROM docteur,employes WHERE docteur.numero="+tableauDocteur.get(i).get(j)+" AND employes.numero="+tableauDocteur.get(i).get(j));
						tableauDocteur3.add(tableauDocteur2.get(j));
						
					}
				}
			}
			
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
		
		return tableauDocteur3;
			
	}
	
	
}
