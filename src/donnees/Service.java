package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Service {
	
	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	private String code_service;
	private String nom_service,batiment_service;
	private int directeur_service;
	
	/**getters*/
	public String getCode_service() {
		return this.code_service;
	}
	
	public String getNom_service() {
		return this.nom_service;
	}
	
	public String getBatiment_service() {
		return this.batiment_service;
	}
	
	public int getDirecteur_service() {
		return this.directeur_service;
	}
	
	/**setters*/
	public void setCode_service(String cs) {
		this.code_service=cs;
	}
	
	public void setNom_service(String ns) {
		this.nom_service=ns;
	}
	
	public void setBatiment_service(String bs) {
		this.batiment_service=bs;
	}
	
	public void setDirecteur_service(int ds) {
		this.directeur_service=ds;
	}
	
	
	/**Add query*/
	/**insertion d'un service avec verification necessaire*/
	public String insertionService(String param1,String param2,String param3,int param4,Connexion objetConnexion) {
		
		String exception=null;
		
		/**on verife que le numero du directeur que l'utilisateur demande existe*/
	
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT * FROM docteur WHERE numero="+param4);
			enregistre = new String[verification.size()][2];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					System.out.println(enregistre[i][j]);
					
				}
			}
			
		
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero saisi pour l'insertion d'un docteur");
		}
		
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur, impossible d'inserer le service, le directeur specifie est introuvable";
			System.out.println("erreur tableau 1");
		}
		
		else {
			
			try {
				
				objetConnexion.executeUpdate("INSERT INTO service VALUES ('"+param1+"','"+param2+"','"+param3+"',"+param4+")");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				
				// TODO Auto-generated catch block
				exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
				
			} catch(SQLException e1) {
				
				exception="Erreur, nombre ou type de parametre incorrect !";
			}
			
			
			
		}
		return exception;

	}
	
	/**Delete query */
	/**delete service*/
	public void deleteService(String code,String directeur,Connexion objetConnexion) {
		System.out.println("coucou");
		
		try {
			if(code.equals(null)||code.equals("")) {
				objetConnexion.executeUpdate("DELETE FROM service WHERE directeur='"+directeur+"'");
			}
			else if(directeur.equals(null) || directeur.equals("")) {
				objetConnexion.executeUpdate("DELETE FROM service WHERE code='"+code+"'");
			}
			else {
				objetConnexion.executeUpdate("DELETE FROM service WHERE code='"+code+"'AND directeur='"+directeur+"'");
			}
			
			
			
		}catch(MySQLIntegrityConstraintViolationException e) {
			
		}catch(SQLException e1) {
			
		}
		
		
		
		
	}
	
	/**recherche des services avec gestion des jointures de table*/
	public ArrayList<Vector<String>> rechercheService(Connexion C) {
		
		/**a chaque recherche de service ce resultat apparaitra*/
		/**test requete en dur */
		ArrayList<Vector<String>> tableauService1 = new ArrayList<Vector<String>>();
		
		try {
			
			tableauService1=C.remplirChampsRequete("SELECT service.nom,service.batiment,employes.nom,employes.prenom,docteur.specialite FROM service,employes,docteur WHERE service.directeur=docteur.numero AND employes.numero=docteur.numero");
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauService1;
		//System.out.println("Voici le tableau test : "+tableauTEST);
	}
	
	
	/**recherche service normale avec le nom, le code etc.*/
	public ArrayList<Vector<String>> rechercheService2(JCheckBox userChoiceBox, JTextField userTextSearch, Connexion connectObject) {
		
		ArrayList<Vector<String>> tableauService2 = new ArrayList<Vector<String>>();

		try {
		
			tableauService2=connectObject.remplirChampsRequete("SELECT * FROM service WHERE "+userChoiceBox.getText()+"='"+userTextSearch.getText()+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauService2;
	}
	
	/**Update query*/
	public void updateService(String code, String nom, String batiment, String directeur, Connexion objetConnexion) {
		
		ArrayList<Vector<String>> test = null;
		
		try {
			test=objetConnexion.remplirChampsRequete("SELECT code FROM service WHERE code='"+code+"'");
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String essai;
		
		essai=	test.get(0).get(0).toString();
		
		System.out.println(essai);
		
		if(essai.equals(code)) {
			try {
					if(nom.equals(null)||nom.equals("")) {
						
						if(batiment.equals(null)||batiment.equals("")) {
							objetConnexion.executeUpdate("UPDATE service SET directeur='"+directeur+"' WHERE code='"+code+"'");
						}
						else if(directeur.equals(null)||directeur.equals("")) {
							objetConnexion.executeUpdate("UPDATE service SET batiment='"+batiment+"' WHERE code='"+code+"'");
						}
						else {
							objetConnexion.executeUpdate("UPDATE service SET batiment='"+batiment+"', directeur='"+directeur+"' WHERE code='"+code+"'");
						}
					}
					else if(batiment.equals(null)||batiment.equals("")) {
						
						if(nom.equals(null)||nom.equals("")) {
							objetConnexion.executeUpdate("UPDATE service SET directeur='"+directeur+"' WHERE code='"+code+"'");
						}
						else if(directeur.equals(null)||directeur.equals("")) {
							objetConnexion.executeUpdate("UPDATE service SET nom='"+nom+"' WHERE code='"+code+"'");
						}
						else {
							objetConnexion.executeUpdate("UPDATE service SET nom='"+nom+"', directeur='"+directeur+"' WHERE code='"+code+"'");
						}
				
					}
					else if(directeur.equals(null)||directeur.equals("")) {
						
						if(nom.equals(null)||nom.equals("")) {
							objetConnexion.executeUpdate("UPDATE service SET batiment='"+batiment+"' WHERE code='"+code+"'");
						}
						else if(batiment.equals(null)||batiment.equals("")) {
							objetConnexion.executeUpdate("UPDATE service SET nom='"+nom+"' WHERE code='"+code+"'");
						}
						else {
							objetConnexion.executeUpdate("UPDATE service SET nom='"+nom+"', batiment='"+batiment+"' WHERE code='"+code+"'");
						}
				
					}
					else {
						objetConnexion.executeUpdate("UPDATE service SET nom='"+nom+"', batiment='"+batiment+"', directeur='"+directeur+"' WHERE code='"+code+"'");
					}
			}catch (MySQLIntegrityConstraintViolationException e) {
				// TODO Auto-generated catch block
				
			} catch(SQLException e1) {
				javax.swing.JOptionPane.showMessageDialog(null,"Merci de rentrer des valeurs coherentes");
			}
		}
		
		else {
			javax.swing.JOptionPane.showMessageDialog(null,"Cet service n'existe pas");
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
