package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Soigne {
	
	/**cle secondaire docteur*/
	int no_docteur;
	/**cle secondaire malade*/
	int no_malade;
	
	/**insertion d'un patient soigne avec verif*/
	public String insertionSoigne(int param1, int param2,Connexion objetConnexion) {

		String exception=null;
		
		/**On verifie que le numero saisi par l'utilisateur correspond a un docteur*/
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		/**verification code service*/
		ArrayList<Vector<String>> verification2 = new ArrayList<Vector<String>>();
		String [][] enregistre2 = null;
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT * FROM docteur WHERE numero="+param1);
			enregistre = new String[verification.size()][2];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					System.out.println(enregistre[i][j]);
					
				}
			}
			
			
			verification2=objetConnexion.remplirChampsRequete("SELECT * FROM malade WHERE numero="+param2);
			enregistre2=new String[verification2.size()][6];
			
			for (int i=0;i<verification2.size();i++) {
				for(int j=0;j<verification2.get(i).size();j++) {
					
					enregistre2[i][j]=verification2.get(i).get(j).toString();
					System.out.println("tableau 2 :"+enregistre2[i][j]);
					
				}
			}

			
			
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero saisi pour l'insertion d'un docteur");
		}
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur le docteur specifie est introuvable";
		}
		
		else if(verification2.size()==0) {
			exception="Erreur, le numero de malade specifie est introuvable";
		}
		
		else {
			
			try {
				
				objetConnexion.executeUpdate("INSERT INTO soigne VALUES ("+param1+","+param2+")");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				
				// TODO Auto-generated catch block
				exception="Erreur, tentative d'insertion d'\u00e9l\u00e9ments d\u00e9ja pr\u00e9sents !";
				
			} catch(SQLException e1) {
				
				exception="Erreur, nombre, type ou valeur de parametre incorrect !";
				//e1.printStackTrace();
			}
			
		}
		
		return exception;
	}
	
	
	/**recherche des soigne avec le no_malade (jointures entre table)*/
	public ArrayList<Vector<String>> rechercheSoigne(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauSoigne1 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne2 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne3 = new ArrayList<Vector<String>>();
		
		
		try {
			
			tableauSoigne1=C.remplirChampsRequete("SELECT no_malade FROM soigne WHERE no_docteur='"+userTextSearch.getText()+"'");
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		try {
			if(tableauSoigne1.size()!=0) {
				
				
				for(int i=0; i<tableauSoigne1.size(); i++)
				{
					for(int j=0; j<tableauSoigne1.get(i).size(); j++)
					{
						System.out.println(tableauSoigne1.get(i).get(j));	
						tableauSoigne2=C.remplirChampsRequete("SELECT soigne.no_docteur,soigne.no_malade,malade.nom,malade.prenom,malade.adresse,malade.mutuelle FROM soigne,malade WHERE no_docteur='"+userTextSearch.getText()+"' AND numero="+tableauSoigne1.get(i).get(j));
						tableauSoigne3.add(tableauSoigne2.get(i));
					}
				}
			}
				
				
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauSoigne3;
		
	}
	
	/**recherche soigne par le numero docteur (avec joitures entre tables)*/
	public ArrayList<Vector<String>> rechercheSoigne2 (JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C){
		
		ArrayList<Vector<String>> tableauSoigne4 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne5 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne6 = new ArrayList<Vector<String>>();
		
		
		try {
			
			tableauSoigne4=C.remplirChampsRequete("SELECT no_docteur FROM soigne WHERE no_malade='"+userTextSearch.getText()+"'");
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		try {
			
			
			if(tableauSoigne4.size()!=0) {
				
				
				for(int i=0; i<tableauSoigne4.size(); i++)
				{
					for(int j=0; j<tableauSoigne4.get(i).size(); j++)
					{
						System.out.println(tableauSoigne4.get(i).get(j));	
						
						tableauSoigne5=C.remplirChampsRequete("SELECT soigne.no_docteur,employes.nom,employes.prenom,soigne.no_malade, malade.nom,malade.prenom,malade.adresse,malade.mutuelle FROM soigne,malade,employes WHERE no_malade='"+userTextSearch.getText()+"' AND malade.numero='"+userTextSearch.getText()+"' AND employes.numero="+tableauSoigne4.get(i).get(j));
						tableauSoigne6.add(tableauSoigne5.get(i));
					}
				}
			}
				
		
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauSoigne6;
		
	}
	
	/**recherche soigne par le nom du malade (bien plus simple je trouve) */
	public ArrayList<Vector<String>> rechercheSoigne3(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		ArrayList<Vector<String>> tableauSoigne7 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne8 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne9 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauSoigne10 = new ArrayList<Vector<String>>();
		
		try {
			
			tableauSoigne7=C.remplirChampsRequete("SELECT numero FROM malade WHERE nom='"+userTextSearch.getText()+"'");
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			if(tableauSoigne7.size()!=0) tableauSoigne8=C.remplirChampsRequete("SELECT no_docteur FROM soigne WHERE no_malade="+tableauSoigne7.get(0).get(0));
		
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			
			if(tableauSoigne8.size()!=0) {
				
				
				for(int i=0; i<tableauSoigne8.size(); i++)
				{
					for(int j=0; j<tableauSoigne8.get(i).size(); j++)
					{
						System.out.println(tableauSoigne8.get(i).get(j));	
						
						tableauSoigne9=C.remplirChampsRequete("SELECT soigne.no_docteur,employes.nom,employes.prenom,soigne.no_malade,malade.nom,malade.prenom,malade.adresse,malade.mutuelle FROM soigne,employes,malade WHERE malade.numero="+tableauSoigne7.get(0).get(0)+" AND soigne.no_malade="+tableauSoigne7.get(0).get(0)+" AND employes.numero="+tableauSoigne8.get(i).get(j));
						tableauSoigne10.add(tableauSoigne9.get(i));
					}
				}
			}
			
			
		}catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		return tableauSoigne10;
	}
	

}
