package donnees;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;

import controleur.Connexion;

public class Hospitalisation {

	/**
	 * We manage an hospital database, therefore, 
	 * I consider that all the attributes must be private.  
	 */
	
	
	private int no_malade;
	/**Cle secondaire de service*/
	private String code_service;
	/**Cle secondaire de chambre*/
	int no_chambre;
	private int lit;
	
	
	/**getter No malade
	 * @return int No Malade*/
	public int getNo_malade() {
		return this.no_malade;
	}
	/**getter de Nb lits
	 * @return int Nb Lits*/
	public int getLit() {
		return this.lit;
	}
	
	
	/**setter No Malade
	 * @param num_malade Numero de malade*/
	public void setNo_malade(int num_malade) {
		this.no_malade=num_malade;
	}
	
	/**setter Nb lits
	 * @param l Nb lits*/
	public void setLit(int l) {
		this.lit=l;
	}
	
	
	/**Add query*/
	/**insertion de l'hospitalisation avec toutes les verifs necessaires*/
	public String insertionHospitalisation(int param1,String param2,int param3,int param4,Connexion objetConnexion) {
		
		String exception=null;
		
		/**verification du numero de malade*/
		ArrayList<Vector<String>> verification = new ArrayList<Vector<String>>();
		String [][] enregistre = null;
		
		/**verification code service*/
		ArrayList<Vector<String>> verification2 = new ArrayList<Vector<String>>();
		String [][] enregistre2 = null;
		
		/**verification le numero de la chambre*/
		ArrayList<Vector<String>> verification3 = new ArrayList<Vector<String>>();
		String [][] enregistre3 = null;
		
		/**verifier que la chambre fait bien partie du service specifie*/
		ArrayList<Vector<String>> verification4 = new ArrayList<Vector<String>>();
		String [][] enregistre4 = null;
		
		/**verifier que le numero de lit n'est pas superieur au nombre de lit de la chambre specifie*/
		//ArrayList<Vector<String>> verification5 = new ArrayList<Vector<String>>();
		//String [][] enregistre5 = null;
		
		
		try {
			
			verification=objetConnexion.remplirChampsRequete("SELECT * FROM malade WHERE numero="+param1);
			enregistre = new String[verification.size()][6];
			
			for (int i=0;i<verification.size();i++) {
				for(int j=0;j<verification.get(i).size();j++) {
					
					enregistre[i][j]=verification.get(i).get(j).toString();
					System.out.println("tableau 1 : "+enregistre[i][j]);
					
				}
			}
			
			
			verification2=objetConnexion.remplirChampsRequete("SELECT * FROM service WHERE code='"+param2+"'");
			enregistre2=new String[verification2.size()][4];
			
			for (int i=0;i<verification2.size();i++) {
				for(int j=0;j<verification2.get(i).size();j++) {
					
					enregistre2[i][j]=verification2.get(i).get(j).toString();
					System.out.println("tableau 2 :"+enregistre2[i][j]);
					
				}
			}
			
			
			verification3=objetConnexion.remplirChampsRequete("SELECT * FROM chambre WHERE no_chambre="+param3);
			enregistre3=new String[verification3.size()][4];
			
			for (int i=0;i<verification3.size();i++) {
				for(int j=0;j<verification3.get(i).size();j++) {
					
					enregistre3[i][j]=verification3.get(i).get(j).toString();
					System.out.println("tableau 3 :"+enregistre3[i][j]);
					
				}
			}
			
			verification4=objetConnexion.remplirChampsRequete("SELECT * FROM chambre WHERE code_service ='"+param2+"'");
			enregistre4=new String[verification4.size()][4];
			
			for (int i=0;i<verification4.size();i++) {
				for(int j=0;j<verification4.get(i).size();j++) {
					
					enregistre4[i][j]=verification4.get(i).get(j).toString();
					System.out.println("tableau 4 :"+enregistre4[i][j]);
					
				}
			}

		
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			//e1.printStackTrace();
			System.out.println("Erreur de verification du numero ou du service saisi pour l'insertion d'un infirmier");
		}
		
		
		/**si le tableau est vide*/
		if(verification.size()==0) {
			
			exception="Erreur, le numero de malade specifie est introuvable";
			System.out.println("erreur tableau 1");
		}
		
		else if(verification2.size()==0) {
			
			exception="Erreur, le service precise n'existe pas";
			System.out.println("erreur tableau 2");
			
		}
		
		else if(verification3.size()==0) {
			
			exception="Erreur, la chambre specifie n'existe pas";
			System.out.println("erreur tableau 3");
			
		}
		
		else if(verification4.size()==0) {
			
			exception="Erreur, la chambre ne fait pas partie du service specifie";
			System.out.println("erreur tableau 4");
			
		}
		
		else {
			
			try {
				
				objetConnexion.executeUpdate("INSERT INTO hospitalisation VALUES ("+param1+",'"+param2+"',"+param3+","+param4+")");
				exception="Insertion was successfully made";
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				
				// TODO Auto-generated catch block
				exception="Erreur, tentive d'insertion d'elements deja presents!";
				
			} catch(SQLException e1) {
				
				exception="Nombre ou type de parametre incorrect, veuillez r\u00e9eessayer !";
			}
			
		}
		return exception;
	}
	
	
	/**Delete query*/
	/**delete hospitalisation avec gestion des delete qui en decoulent*/
	public void deleteHospitalisation(String numero,Connexion objetConnexion) {
	
		try {
			
				objetConnexion.executeUpdate("DELETE FROM hospitalisation WHERE no_malade='"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM malade WHERE numero='"+numero+"'");
				objetConnexion.executeUpdate("DELETE FROM soigne no_malade='"+numero+"'");
				
			
			
		}catch(MySQLIntegrityConstraintViolationException e) {
			
		}catch(SQLException e1) {
			
		}
		
	}
	
	
	
	/**recherche de l'hospitalisation avec gestion des jointures de tables*/
	public ArrayList<Vector<String>> rechercheHospitalisation(JCheckBox userChoiceBox, JTextField userTextSearch,Connexion C) {
		
		/**a chaque recherche de service ce resultat apparaitra*/
		
		ArrayList<Vector<String>> tableauHospitalisation1 = new ArrayList<Vector<String>>();
		ArrayList<Vector<String>> tableauHospitalisation2 = new ArrayList<Vector<String>>();
		
		try {
			
			tableauHospitalisation2=C.remplirChampsRequete("SELECT numero FROM malade WHERE nom='"+userTextSearch.getText()+"'");
			
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		try {
			if(tableauHospitalisation2.size()!=0) tableauHospitalisation1=C.remplirChampsRequete("SELECT hospitalisation.code_service,hospitalisation.no_chambre,hospitalisation.lit,malade.nom,malade.prenom,malade.adresse,malade.mutuelle FROM hospitalisation,malade WHERE hospitalisation.no_malade="+tableauHospitalisation2.get(0).get(0)+" AND malade.numero="+tableauHospitalisation2.get(0).get(0));
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tableauHospitalisation1;
		//System.out.println("Voici le tableau test : "+tableauTEST);
	}

	
	/**Update Query*/
	/**update de l'hospitalisation */
	public void updateHospitalisation(String numero, String service, String chambre, String lit, Connexion objetConnexion) {
		
		ArrayList<Vector<String>> test = null;
		
		try {
			test=objetConnexion.remplirChampsRequete("SELECT no_malade FROM hospitalisation WHERE no_malade='"+numero+"'");
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String essai;
				
			essai=	test.get(0).get(0).toString();
			
			
		
		System.out.println(essai);
		
		if(essai.equals(numero)) {
			
			try {	
					
					if(service.equals(null)||service.equals("")) {
						
						if(chambre.equals(null)||chambre.equals("")) {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET lit='"+lit+"' WHERE no_malade='"+numero+"'");
						}
						else if(lit.equals(null)||lit.equals("")) {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET no_chambre='"+chambre+"' WHERE no_malade='"+numero+"'");
						}
						else {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET no_chambre='"+chambre+"', lit='"+lit+"' WHERE no_malade='"+numero+"'");
						}
					}
					else if(chambre.equals(null)||chambre.equals("")) {
						
						if(service.equals(null)||service.equals("")) {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET lit='"+lit+"' WHERE no_malade='"+numero+"'");
						}
						else if(lit.equals(null)||lit.equals("")) {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET code_service='"+service+"' WHERE no_malade='"+numero+"'");
						}
						else {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET code_service='"+service+"', lit='"+lit+"' WHERE no_malade='"+numero+"'");
						}
					}
					else if(lit.equals(null)||lit.equals("")) {
						
						if(service.equals(null)||service.equals("")) {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET no_chambre='"+chambre+"' WHERE no_malade='"+numero+"'");
						}
						else if(chambre.equals(null)||chambre.equals("")) {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET code_service='"+service+"' WHERE no_malade='"+numero+"'");
						}
						else {
							objetConnexion.executeUpdate("UPDATE hospitalisation SET code_service='"+service+"', no_chambre='"+chambre+"' WHERE no_malade='"+numero+"'");
						}
					}
					else {
						objetConnexion.executeUpdate("UPDATE hospitalisation SET code_service='"+service+"', no_chambre='"+chambre+"', lit='"+lit+"' WHERE no_malade='"+numero+"'");
						
					}
				
				
				
			} catch (MySQLIntegrityConstraintViolationException e) {
				// TODO Auto-generated catch block
				
			} catch(SQLException e1) {
				javax.swing.JOptionPane.showMessageDialog(null,"Merci de rentrer des valeurs coherentes");
			}
			
		}else {
			javax.swing.JOptionPane.showMessageDialog(null,"Cet infirmier n'existe pas");
		}
		}
	
}
