package vue;

import javax.swing.JProgressBar;

public class ThreadBar extends Thread {
	

	boolean running= true;
	int valeur=0;
	public FenetreConnexion beginPage=new FenetreConnexion();
	JProgressBar a= beginPage.barre;
	int valueBar;
	
	
	public ThreadBar() {
		
	    a.setMaximum(100);
	    a.setMinimum(0);
	    a.setStringPainted(true);
	    beginPage.ecriturePermanante();
	}
	
	
	public void run() {
		
		while(running) {
		
			valeur++;
			a.setValue(valeur);
				
			try {
				Thread.sleep(45);
			}catch(InterruptedException e) {
				System.out.println("Oops une erreur est survenue ");
				this.running=false;
			}
			
			
			System.out.println(this.getBarValue());
			valueBar=this.stopBarre();
			if(valueBar==100){
				
				beginPage.dessiner();
				
			}
	
		}
		
	}
	
	
	public int stopBarre() {
		
		if(valeur==100) {
			
			this.running=false;	
		}
		System.out.println("Le sous programme stop barre renvoie la valeur : "+valeur);
		System.out.println((this.getState()));
		return valeur;
	}
	
	
	public int getBarValue() {
		return valeur;
	}
	
}
