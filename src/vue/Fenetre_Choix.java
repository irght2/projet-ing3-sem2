package vue;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Fenetre_Choix extends JFrame implements WindowListener{
	
	ButtonGroup grp_choix;
	public JRadioButton btn_ssh, btn_local;
	JPanel north, south;
	public JPanel west;
	public JButton btn_connexion;
	JLabel label_id, label_mdp;
	public JLabel label2_id, label2_mdp;
	JTextField text_id;
	public JTextField text2_id;
	JPasswordField mdp;
	public JPasswordField mdp2;
	String defaut = "root";
	
	public Fenetre_Choix()
	{
		super("Choisissez le type de connexion");
		this.addWindowListener(this);
		this.setLayout(new BorderLayout());
		
		/**Panel North, avec RadioButton*/
		north = new JPanel();
		north.setLayout(new FlowLayout());
		
		grp_choix = new ButtonGroup();
		btn_ssh = new JRadioButton("SSH");
		
		btn_local = new JRadioButton("Local");
		btn_local.setSelected(true);
		
		grp_choix.add(btn_ssh);
		grp_choix.add(btn_local);
		
		north.add(btn_ssh);
		north.add(btn_local);
		
		/**Panel West avec form pour connexion SSH*/
		west = new JPanel();
		west.setLayout(new GridLayout(0,2));
		
		label_id = new JLabel("Identifiant : ");
		west.add(label_id);
		text_id = new JTextField(15);
		//text_id.setText(defaut);
		west.add(text_id);
		
		label_mdp = new JLabel("Mot de passe :");
		west.add(label_mdp);
		mdp = new JPasswordField(15);
		//mdp.setText(defaut);
		west.add(mdp);
		
		/**Panel South, avec bouton connexion*/
		south = new JPanel();
		
		btn_connexion = new JButton("Connexion");
		
		south.add(btn_connexion);
		
		
		/**Ajout des panels au Frame*/
		this.add(north, BorderLayout.NORTH);
		this.add(west, BorderLayout.WEST);
		this.add(south, BorderLayout.SOUTH);
		
		this.pack();
		this.setVisible(true);
		this.setSize(400, 150);
		this.setLocation(500, 200);
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	public String getTextId()
	{
		return this.text_id.getText();
	}
	
	public String getTextPassword()
	{
		return String.valueOf(this.mdp.getPassword());
	}

}
