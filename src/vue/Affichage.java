package vue;

public class Affichage extends controleur.Main {
	
	/**Creating fenetre_principal object*/
	public Fenetre_Principale fp; 
	public Fenetre_Choix fc;
	public FenetreConnexion fenC;
	public ThreadBar monThread;
	
	public void newFenetre_principale() {
		fp= new Fenetre_Principale();
	}
	
	public void newFenetre_choix() {
		fc= new Fenetre_Choix();
		
	}
	
	public void newFenetreConnexion() {
		
		
		monThread=new ThreadBar();
		monThread.start();
		monThread.beginPage.addKeyListener(this);
		
	}
	
	public void ajoutActionListenerPrincipale() {
		
		/**Menu deroulant edition */
		fp.modifier.addActionListener(this);
		fp.inserer.addActionListener(this);
		fp.supprimer.addActionListener(this);
		
		/**Menu deroulant aide*/
		fp.aide_modifier.addActionListener(this);
		fp.aide_inserer.addActionListener(this);
		fp.aide_supprimer.addActionListener(this);
		fp.aide_rechercher.addActionListener(this);
		
		/**Panel west onglet recherche*/
		fp.btn_service.addActionListener(this);
		fp.btn_chambre.addActionListener(this);
		fp.btn_employe.addActionListener(this);
		fp.btn_malade.addActionListener(this);
		fp.btn_hospitalisation.addActionListener(this);
		fp.btn_infirmier.addActionListener(this);
		fp.btn_docteur.addActionListener(this);
		fp.btn_soigne.addActionListener(this);
		fp.btn_rdv.addActionListener(this);
		fp.btn_recherche.addActionListener(this);
		
		/**Panel west onglet parametre*/
		fp.champ1.addItemListener(this);
		fp.champ2.addItemListener(this);
		fp.champ3.addItemListener(this);
		fp.champ4.addItemListener(this);
		
		/**Panel RDV*/
		fp.btn_save_rdv.addActionListener(this);
		
		/**Bouton quitter*/
		fp.quitter.addActionListener(this);
		
		
	}
	
	public void ajoutActionListenerChoix() {
		
		/**choix*/
		fc.btn_ssh.addActionListener(this);
		fc.btn_local.addActionListener(this);
		
		/**Connexion*/
		fc.btn_connexion.addActionListener(this);
		
	}
	
}
