package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
//import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;


@SuppressWarnings("serial")
public class Fenetre_Principale extends JFrame implements WindowListener {
	
	JPanel north, west, west1, center, south, panel_reporting, panel_rdv, panel_rdv1, panel_recherche, panel_reporting1, panel_reporting2, panel_reporting3, panel_reporting4;
	public JPanel panel_donnee, west2, panel_param;
	JTabbedPane tabbedPane;
	JTextField heure_rdv, date_rdv, n_patient, n_docteur;
	JMenuBar menu1, menu2;
	JMenu edition, aide;
	public JMenuItem modifier, inserer, supprimer, aide_modifier, aide_inserer, aide_supprimer,aide_rechercher;
	public ButtonGroup groupe_recherche;
	public JRadioButton btn_service, btn_chambre, btn_employe, btn_malade, btn_hospitalisation, btn_rdv, btn_infirmier, btn_docteur, btn_soigne;
	public JTextField text_recherche1, text_recherche2, text_recherche3, text_recherche4;
	public JLabel cle_recherche1, cle_recherche2, cle_recherche3, cle_recherche4, label_heure, label_date, label_patient, label_docteur;
	JTable table;
	public JCheckBox champ1, champ2, champ3, champ4;
	public JButton quitter, btn_recherche, btn_save_rdv;
	JSeparator separateur;
	
	public JPanel panel_donnee1,panel_donnee2;
	
	public Fenetre_Principale()
	{
		super("Gestion de Base de donn\u00e9e d'Hopital");
		this.addWindowListener(this);
		this.setLayout(new BorderLayout());
		
		/**Panel North, avec menu deroulant*/
		
		north = new JPanel();
		FlowLayout layout = new FlowLayout();
		layout.setAlignment(FlowLayout.LEFT);
		north.setLayout(layout);
		
		/**Menu Edition*/
		menu1 = new JMenuBar();
		edition = new JMenu("Edition");
		modifier = new JMenuItem("Modifier");
		inserer = new JMenuItem("Inserer");
		supprimer = new JMenuItem("Supprimer");
		
		edition.add(modifier);
		edition.add(inserer);
		edition.add(supprimer);
		
		menu1.add(edition);
		
		/**Menu Aide*/
		menu2 = new JMenuBar();
		aide = new JMenu("Aide");
		aide_modifier = new JMenuItem("Comment modifier ?");
		aide_inserer = new JMenuItem("Comment inserer ?");
		aide_supprimer = new JMenuItem("Comment supprimer ?");
		aide_rechercher = new JMenuItem("Comment rechercher ?");
		
		aide.add(aide_modifier);
		aide.add(aide_inserer);
		aide.add(aide_supprimer);
		aide.add(aide_rechercher);
		
		menu2.add(aide);
		
		north.add(menu1);
		north.add(menu2);
				
		/**Panel Ouest, avec onglet recherche*/
		west = new JPanel();		//Panel global west
		west.setLayout(new BorderLayout());
		west1 = new JPanel();	//Panel ouest du haut
		west1.setLayout(new BorderLayout());
		west2 = new JPanel();	//Panel ouest du bas
		west2.setLayout(new GridLayout(0,1));
		panel_recherche= new JPanel();
		panel_recherche.setLayout(new GridLayout(0,1));
		panel_param = new JPanel();
		panel_param.setLayout(new GridLayout(0,1));
		west.setBorder(new TitledBorder("Recherche :"));
		
		//Panel ouest haut gauche, panel_recherche
		
		groupe_recherche = new ButtonGroup();
		
		btn_service = new JRadioButton("Service");
		groupe_recherche.add(btn_service);

		btn_chambre = new JRadioButton("Chambre");
		groupe_recherche.add(btn_chambre);
		
		btn_employe = new JRadioButton("Employes");
		groupe_recherche.add(btn_employe);
		
		btn_malade = new JRadioButton("Malade");
		btn_malade.setSelected(true);
		groupe_recherche.add(btn_malade);
		
		btn_hospitalisation = new JRadioButton("Hospitalisation");
		groupe_recherche.add(btn_hospitalisation);
		
		btn_rdv = new JRadioButton("Rendez-vous");
		groupe_recherche.add(btn_rdv);
		
		btn_infirmier = new JRadioButton("Infirmier");
		groupe_recherche.add(btn_infirmier);
		
		btn_docteur = new JRadioButton("Docteur");
		groupe_recherche.add(btn_docteur);
		
		btn_soigne = new JRadioButton("Soigne");
		groupe_recherche.add(btn_soigne);

		panel_recherche.add(btn_service);
		panel_recherche.add(btn_chambre);
		panel_recherche.add(btn_employe);
		panel_recherche.add(btn_malade);
		panel_recherche.add(btn_hospitalisation);
		panel_recherche.add(btn_rdv);
		panel_recherche.add(btn_infirmier);
		panel_recherche.add(btn_docteur);
		panel_recherche.add(btn_soigne);
		
		separateur = new JSeparator(SwingConstants.VERTICAL);
		//separateur.setSize(10, 10);
		
		west1.add(panel_recherche, BorderLayout.WEST);
		west1.add(separateur, BorderLayout.CENTER);
		
		
		//Panel ouest haut droit, panel_param
		champ1 = new JCheckBox("numero");
		champ1.setSelected(true);
		champ2 = new JCheckBox("nom");
		champ2.setSelected(true);
		champ3 = new JCheckBox("prenom");
		champ3.setSelected(true);
		champ4 = new JCheckBox("mutuelle");
		
		panel_param.add(champ1);
		panel_param.add(champ2);
		panel_param.add(champ3);
		panel_param.add(champ4);
		
		west1.add(panel_param, BorderLayout.EAST);
		
		//Panel ouest base, west2
		
		cle_recherche1 = new JLabel("Num\u00e9ro :");
		cle_recherche2 = new JLabel("Nom :");
		cle_recherche3 = new JLabel("Pr\u00e9nom :");
		cle_recherche4 = new JLabel("Mutuelle");
		text_recherche1 = new JTextField(10);
		text_recherche2 = new JTextField(10);
		text_recherche3 = new JTextField(10);
		text_recherche4 = new JTextField(10);
		west2.add(cle_recherche1);
		west2.add(text_recherche1);
		west2.add(cle_recherche2);
		west2.add(text_recherche2);
		west2.add(cle_recherche3);
		west2.add(text_recherche3);
		west2.add(cle_recherche4);
		west2.add(text_recherche4);
		
		btn_recherche = new JButton("Rechercher");
		west.add(west1, BorderLayout.NORTH);
		west.add(west2, BorderLayout.CENTER);
		west.add(btn_recherche, BorderLayout.SOUTH);
				
		/**Panel East, avec 3 onglets*/
		center = new JPanel();
		center.setLayout(new GridLayout(0,1));
		
		/**Panel Donnee*/
		
		
		panel_donnee = new JPanel();
		panel_donnee.setLayout(new BorderLayout());
		
		//panel_donnee.setLayout(new GridLayout(0,1));
		//panel_donnee1 = new JPanel();
		//panel_donnee1.setLayout(new BorderLayout());
		
		//panel_donnee2 = new JPanel();
		//panel_donnee2.setLayout(new BorderLayout());
		
		//panel_donnee.add(panel_donnee1,BorderLayout.NORTH);
		//panel_donnee.add(panel_donnee2,BorderLayout.CENTER);
		
		
		
        /**Panel Reporting*/
		panel_reporting = new JPanel();
		panel_reporting.setLayout(new GridLayout(2,2));
		panel_reporting1 = new JPanel();
		panel_reporting1.setLayout(new FlowLayout());
		panel_reporting2 = new JPanel();
		panel_reporting2.setLayout(new FlowLayout());
		panel_reporting3 = new JPanel();
		panel_reporting3.setLayout(new FlowLayout());
		panel_reporting4 = new JPanel();
		panel_reporting4.setLayout(new FlowLayout());
		
		panel_reporting.add(panel_reporting1);
		panel_reporting.add(panel_reporting2);
		panel_reporting.add(panel_reporting3);
		panel_reporting.add(panel_reporting4);
		
		
		/**Panel RDV*/
		panel_rdv = new JPanel();
		panel_rdv.setLayout(new BorderLayout());
		
		panel_rdv1 = new JPanel();
		panel_rdv1.setLayout(new GridLayout(0,2));
		
		btn_save_rdv = new JButton("Enregistrer");
		
		label_heure = new JLabel("Heure du RDV :");
		heure_rdv = new JTextField(15);
		label_date = new JLabel("Date du RDV :");
		date_rdv = new JTextField(15);
		label_patient = new JLabel("N° du patient :");
		n_patient = new JTextField(15);
		label_docteur = new JLabel("N° du docteur :");
		n_docteur = new JTextField(15);
		
		panel_rdv1.add(label_heure);
		panel_rdv1.add(heure_rdv);
		panel_rdv1.add(label_date);
		panel_rdv1.add(date_rdv);
		panel_rdv1.add(label_patient);
		panel_rdv1.add(n_patient);
		panel_rdv1.add(label_docteur);
		panel_rdv1.add(n_docteur);
		
		panel_rdv.add(panel_rdv1, BorderLayout.CENTER);
		panel_rdv.add(btn_save_rdv, BorderLayout.SOUTH);

		/**Creation des onglets*/
		tabbedPane= new JTabbedPane();
		tabbedPane.addTab("Donn\u00e9es", null, panel_donnee, "Onglet des donn\u00e9es");
		tabbedPane.addTab("Reporting", null, panel_reporting, "Onglet du reporting");
		tabbedPane.addTab("RDV", null, panel_rdv, "Onglet de prise de rendez-vous");

		
		center.add(tabbedPane);
				
		/**Bouton quitter*/
		south = new JPanel();
		quitter = new JButton("Quitter");
		south.add(quitter);
		
		/**Ajout des panels au Frame*/
		this.add(north, BorderLayout.NORTH);
		this.add(west, BorderLayout.WEST);
		this.add(center, BorderLayout.CENTER);
		this.add(south, BorderLayout.SOUTH);
		
		this.pack();
		this.setSize(1000, 700);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	/**draw reporting1*/
    public void drawcamemberttest(float a, float b, float c, float d, float e,float f) {
        // create a dataset...
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("Orthopediste", a);
        dataset.setValue("Cardiologue", b);
        dataset.setValue("Traumatologue",c);
        dataset.setValue("Anesthesiste", d);
        dataset.setValue("Radiologue", e);
        dataset.setValue("Pneumologue", f);
        
        // create a chart...
        JFreeChart chart = ChartFactory.createPieChart(
        "Docteurs par sp\u00e9cialit\u00e9s",
        dataset,
        true,    // legend?
        true,    // tooltips?
        false    // URLs?
        );
        // create and display a frame...
       // ChartFrame frame = new ChartFrame("First", chart);
        
     // create and display a panel...
        ChartPanel panel = new ChartPanel(chart);
        /**On met aux bonnes dimensions*/
        panel.setPreferredSize(new java.awt.Dimension(panel_reporting1.getWidth(), panel_reporting1.getHeight()));
        panel_reporting1.add(panel);

        
       // frame.pack();
       // frame.setVisible(true);
    }

    /**draw reporting3*/
    public void drawcamembert(float a, float b, float c, float d, float e,float f,float g,float h,float i,float j,float k,float l) {
        // create a dataset...
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("LMDE", a);
        dataset.setValue("MNH", b);
        dataset.setValue("MAAF",c);
        dataset.setValue("MGEN", d);
        dataset.setValue("MMA", e);
        dataset.setValue("MNFTC", f);
        dataset.setValue("MNAM", g);
        dataset.setValue("MGSP", h);
        dataset.setValue("MAS", i);
        dataset.setValue("CNAMTS", j);
        dataset.setValue("CCVRP", k);
        dataset.setValue("AG2R", l);
        
        
        
        // create a chart...
        JFreeChart chart = ChartFactory.createPieChart(
        "Malades par mutuelles",
        dataset,
        true,    // legend?
        true,    // tooltips?
        false    // URLs?
        );
        // create and display a frame...
       // ChartFrame frame = new ChartFrame("First", chart);
        
     // create and display a panel...
        ChartPanel panel = new ChartPanel(chart);
        /**On met aux bonnes dimensions*/
        panel.setPreferredSize(new java.awt.Dimension(panel_reporting3.getWidth(), panel_reporting3.getHeight()));
        panel_reporting3.add(panel);

        
       // frame.pack();
       // frame.setVisible(true);
    }
    
    /**draw reporting2*/
    public void drawpieinfirmier(float a, float b, float c) {
        // create a dataset...
        DefaultPieDataset dataset = new DefaultPieDataset();
        dataset.setValue("CHG", a);
        dataset.setValue("REA", b);
        dataset.setValue("OPH",c);
        
        
        
        
        // create a chart...
        JFreeChart chart = ChartFactory.createPieChart(
        "Infirmiers par service",
        dataset,
        true,    // legend?
        true,    // tooltips?
        false    // URLs?
        );
        // create and display a frame...
       // ChartFrame frame = new ChartFrame("First", chart);
        
     // create and display a panel...
        ChartPanel panel = new ChartPanel(chart);
        /**On met aux bonnes dimensions*/
        panel.setPreferredSize(new java.awt.Dimension(panel_reporting4.getWidth(), panel_reporting4.getHeight()));
        panel_reporting4.add(panel);

        
       // frame.pack();
       // frame.setVisible(true);
    }
    
    /**draw reporting4*/
    public void drawbartest(float a,float b,float c,float d,float e) {
    	
    	DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    	dataset.addValue(a, "JOUR", "Nombre d'infirmiers");
    	dataset.addValue(b, "NUIT", "Nombre d'infirmiers");
    	dataset.addValue(c, "REA", "Service");
    	dataset.addValue(d, "CHG", "Service");
    	dataset.addValue(e, "OPH", "Service");
    	
    	JFreeChart chart = ChartFactory.createBarChart(
    	"Diagramme des salaires par indicateurs",         // chart title
    	"Salaires",               // domain axis label
    	"Euros",                  // range axis label
    	dataset,                  // data
    	PlotOrientation.VERTICAL, // orientation
    	true,                     // include legend
    	true,                     // tooltips?
    	false                     // URLs?
    	);
    	
    	ChartPanel chartPanel = new ChartPanel(chart, false);
    	panel_reporting2.add(chartPanel);
    	
        //On met aux bonnes dimensions
        chartPanel.setPreferredSize(new java.awt.Dimension(panel_reporting2.getWidth(), panel_reporting2.getHeight()));
        
    }
}
