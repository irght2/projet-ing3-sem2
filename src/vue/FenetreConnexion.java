package vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;


@SuppressWarnings("serial")
public class FenetreConnexion extends JFrame implements WindowListener {
	
	public JLabel image;
	public JLabel b;
	int value=0;
	JProgressBar barre;
	ThreadBar monThread;
		
	public FenetreConnexion() {
		
		super("Take care of your medical data");
		this.setLayout(new BorderLayout());
		
		/**JProgress bar*/
		barre=new JProgressBar();;
		
		/**On charge l'image pour l'affichage de l'image a gauche du menu*/
		ImageIcon icone = new ImageIcon("images/health.jpg");
		image = new JLabel(icone);		
		add(image);
		
		this.add(barre,BorderLayout.SOUTH);
		this.pack();
		this.setLocationRelativeTo(null);
		this.setFocusable(true);
		this.setVisible(true);

	}
	
	
	public void dessiner() {
		Graphics g;
		g=this.getGraphics();
		g.setColor(Color.white);
		g.setFont(new Font("TimesRoman", /*Font.BOLD+*/Font.ITALIC, 25));
		g.drawString("Appuyer sur n'importe quelle touche pour vous connecter ...", 120,570);
		
	}
	
	public void ecriturePermanante() {
		
		Graphics g1;
		g1=this.getGraphics();
		g1.setColor(Color.white);
		//g1.setFont(new Font("TimesRoman", Font.BOLD+Font.ITALIC, 30));
		g1.setFont(new Font("Courier",Font.ITALIC+Font.BOLD,40));
		g1.drawString("GEREZ en toute SIMPLICITE", 80, 210);
		
		g1.setFont(new Font("Courier",Font.BOLD,20));
		g1.drawString("Logiciel de gestion de base de donnees", 80, 250);
			
	}
	
	
	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(0);
	}


	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


}
