package controleur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;


/**import donnees.Classe*/
import donnees.Chambre;
import donnees.Docteur;
import donnees.Employe;
import donnees.Hospitalisation;
import donnees.Infirmier;
import donnees.Malade;
import donnees.RDV;
import donnees.Service;
import donnees.Soigne;
import vue.Affichage;

/**Thanks to https://code.i-harness.com/fr/q/31247 for having shown us how to get selected RadioButton*/

public class Main implements ActionListener, WindowListener, KeyListener, ItemListener {
	
	/**Class declaration*/
	static Connexion connexion;
	static vue.Affichage affiche;
	
	/**JButton and JFrame edition window*/
	JTextField text_edition1,text_edition2,text_edition3,text_edition4,text_edition5,text_edition6;
	JButton btn_variable,btn_annuler;
	JFrame fenEditionMenu;
	JPanel panelSouthTotal,panelCenter_Edition,panelSouthException;
	JLabel writeException;
	
	/**Variable declaration*/
	//static String mdp = "2CE8dbad7a+";
	//static String id = "cs152104";
	boolean valid_password=false;
	static ArrayList liste = null;
	
	/**Class object*/
	Chambre chambre = new Chambre();
	Docteur docteur = new Docteur();
	Employe employe = new Employe();
	Hospitalisation hospitalisation = new Hospitalisation();
	Infirmier infirmier = new Infirmier();
	Malade malade = new Malade();
	RDV rdv = new RDV();
	Service service = new Service();
	Soigne soigne = new Soigne();

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		affiche=new Affichage();
		affiche.newFenetreConnexion();
	
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(0);
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
				/**Si bouton local, pas besoin de champs pour la connection SQL*/
				if(e.getSource().equals(affiche.fc.btn_local))
				{
					System.out.println("Connexion en local...");
					affiche.fc.west.remove(affiche.fc.label2_id);
					affiche.fc.west.remove(affiche.fc.label2_mdp);
					affiche.fc.west.remove(affiche.fc.text2_id);
					affiche.fc.west.remove(affiche.fc.mdp2);
										
					affiche.fc.setSize(400, 150);
					
					affiche.fc.revalidate();
					affiche.fc.repaint();
				}
				
				/**Si bouton SSH, ajout des champs SSH*/
				else if(e.getSource().equals(affiche.fc.btn_ssh))
				{
					System.out.println("Connexion en SSH...");
					affiche.fc.label2_id = new JLabel("Identifiant SQL :");
					affiche.fc.west.add(affiche.fc.label2_id);
					affiche.fc.text2_id = new JTextField(15);
					affiche.fc.west.add(affiche.fc.text2_id);
					
					affiche.fc.label2_mdp = new JLabel("Mot de passe SQL :");
					affiche.fc.west.add(affiche.fc.label2_mdp);
					affiche.fc.mdp2 = new JPasswordField(15);
					affiche.fc.west.add(affiche.fc.mdp2);
					
					affiche.fc.setSize(400, 215);
					
					affiche.fc.revalidate();
					affiche.fc.repaint();
				}
				
				/**Local and remote connection*/
				else if(e.getSource().equals(affiche.fc.btn_connexion)) 
				{
					
					try {
						if(affiche.fc.btn_local.isSelected())
						{
							connexion = new Connexion("hopital", affiche.fc.getTextId(), affiche.fc.getTextPassword());
							valid_password=true;
							
						}
						
						if(affiche.fc.btn_ssh.isSelected())
						{
							//connexion = new Connexion(id, mdp, "cs152104", "PPmV8kRl");
						
						}

					} catch (ClassNotFoundException | SQLException e1) {
						// TODO Auto-generated catch block
						
						JOptionPane.showMessageDialog(null, "Permission Denied", "Warning",JOptionPane.ERROR_MESSAGE);
					}
					
					
					/**display table data test*/
					try {
						/*Chambre c302=new Chambre();
						liste = connexion.remplirChampsTable("chambre");
						System.out.println(liste.get(0));
						System.out.println("bouh");
						c302.deleteChambre(102,connexion);
						c302.insertionChambre(1, "yolo", 3,"DAE", connexion);
						System.out.println("bouh2");*/
						Service cardio=new Service();
						/*cardio.deleteService("CAR", "", connexion);
						employe.deleteEmploye("85", "Mathieu", "Paul-Henri", connexion);
						employe.updateEmploye("22", "maman", "papa", "aldric", "", connexion);
						malade.updateMalade("3", "Youri", "", "mchel", "","q7z22", connexion);
						infirmier.updateInfirmier("22","JOUR","1000", connexion);
						hospitalisation.updateHospitalisation("37", "", "403", "5", connexion);*/
						
						
					} catch ( NullPointerException e1) {
						//TODO Auto-generated catch block
						
						System.out.println("Erreur, impossible de lire dans la base de donnees");
					}
					
					
					/**if password and user name are correct, instantiate new main window. Else error */
					if(valid_password==true) {
						
						affiche.newFenetre_principale();
						affiche.fc.dispose();
						affiche.ajoutActionListenerPrincipale();
						
						/**pour l'instant je le met la on verra apres*/
						reporting();
						reporting2();
						reporting3();
						reporting4();
						
					}
				
				}
				
				
				/**Actions Listener de la fenetre principale*/
				else if(e.getSource().equals(affiche.fp.inserer))
				{
					fenetreMenuEdition("Fenetre d'insertion de nouvelles donnees","Inserer les nouvelles donnees");
					//instanceOf(panelCenter_Edition);
					
					
				}
				else if(e.getSource().equals(affiche.fp.modifier))
				{
					
					fenetreMenuModification("Fenetre de modification des donnees","Enregistrer les modifications");
				}
				else if(e.getSource().equals(affiche.fp.supprimer))
				{
					fenetreMenuSuppression("Fenetre de suppression de donnees","Supprimer les donnees");
				}
				else if(e.getSource().equals(affiche.fp.aide_inserer))
				{
					JOptionPane.showMessageDialog(null, "Pour inserer des informations pr\u00e9sentent dans le tableau :\n\n"+
														" - Rendez-vous dans le menu Edition\n"+
														" - Selectionnez Inserer\n"+
														" - Remplissez les nouvelles informations", "Aide", JOptionPane.PLAIN_MESSAGE);			

				}
				else if(e.getSource().equals(affiche.fp.aide_modifier))
				{
					JOptionPane.showMessageDialog(null, "Pour modifier des informations pr\u00e9sentent dans le tableau :\n\n"+
														" - Rendez-vous dans le menu Edition\n"+
														" - Selectionnez Modifier\n"+
														" - Remplissez les nouvelles informations", "Aide", JOptionPane.PLAIN_MESSAGE);			

				}
				else if(e.getSource().equals(affiche.fp.aide_supprimer))
				{
					JOptionPane.showMessageDialog(null, "Pour supprimer des informations pr\u00e9sentent dans le tableau :\n\n"+
														" - Rendez-vous dans le menu Edition\n"+
														" - Selectionnez Supprimer\n"+
														" - Selectionnez les informations \u00e0 supprimer", "Aide", JOptionPane.PLAIN_MESSAGE);			

				}
				
				else if(e.getSource().equals(affiche.fp.aide_rechercher)) {
					
					JOptionPane.showMessageDialog(null, "Pour rechercher des informations pr\u00e9sentent dans votre base de donnees :\n\n"+
							" - Rendez-vous dans le panneau recherche a gauche de la fenetre\n"+
							" - Selectionnez la table dans laquelle vous souhaitez rechercher a l'aide des boutons ronds\n"+
							" - Selectionnez l'attribut sur lequel vous souhaitez effectuer votre recherche a l'aide boutons carres (un � la fois s'il vous plait) \n"+
							" - Remplissez la zone de texte correpondant a l'attribut selectionne et appuyer sur rechercher \n "+
							" - De temps � autre vous verrez apparaitre deux tablaux de resultats : ce sont des recherches effectuees automatiquement par le systeme \n"+
							"   afin de vous donner de plus amples informations \n"+
							" - Ces recherches seront toujours affichees en bas du panneau donnees", "Aide", JOptionPane.PLAIN_MESSAGE);
					
				}
				
				else if(e.getSource().equals(affiche.fp.quitter))
				{
					System.exit(0);
				}
				
				
				
				/**Research button*/
				else if(e.getSource().equals(affiche.fp.btn_recherche))
				{   
					System.out.println("Recherche en cours...");
					
					if(affiche.fp.btn_malade.isSelected()) rechercheMalade();
					else if(affiche.fp.btn_chambre.isSelected()) rechercheChambre();
					else if(affiche.fp.btn_employe.isSelected()) rechercheEmploye();
					else if(affiche.fp.btn_service.isSelected()) rechercheService();
					else if(affiche.fp.btn_hospitalisation.isSelected()) rechercheHospitalisation();
					else if(affiche.fp.btn_soigne.isSelected()) rechercheSoigne();
					else if(affiche.fp.btn_infirmier.isSelected()) rechercheInfirmier();
					else if(affiche.fp.btn_docteur.isSelected()) rechercheDocteur();
				}
				
				
				/**Partie choix de table*/
				else if(e.getSource().equals(affiche.fp.btn_service))
				{
					System.out.println("Vous recherchez un service");
					
					/**On affiche les param possiblent pour la table Service*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("code");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("nom");
					affiche.fp.champ3 = new JCheckBox("batiment");
					affiche.fp.champ4 = new JCheckBox("directeur");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					affiche.fp.champ4.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					affiche.fp.panel_param.add(affiche.fp.champ4);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("Code :");
					affiche.fp.cle_recherche2 = new JLabel("Nom :");
					affiche.fp.cle_recherche3 = new JLabel("Batiment :");
					affiche.fp.cle_recherche4 = new JLabel("Directeur :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					affiche.fp.text_recherche4 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);
					affiche.fp.west2.add(affiche.fp.text_recherche3);
					affiche.fp.west2.add(affiche.fp.cle_recherche4);
					affiche.fp.west2.add(affiche.fp.text_recherche4);
					
					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_employe))
				{
					System.out.println("Vous recherchez un employe");
					
					/**On affiche les param possiblent pour la table Employes*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("numero");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("nom");
					affiche.fp.champ3 = new JCheckBox("prenom");

					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("Num\u00e9ro :");
					affiche.fp.cle_recherche2 = new JLabel("Nom :");
					affiche.fp.cle_recherche3 = new JLabel("Pr\u00e9nom :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);
					affiche.fp.west2.add(affiche.fp.text_recherche3);
					
					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_hospitalisation))
				{
					System.out.println("Vous recherchez une hospitalisation");
					
					/**On affiche les param possiblent pour la table Hospitalisation*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("Nom du malade");
					affiche.fp.champ1.setSelected(true);
					
					affiche.fp.champ1.addItemListener(this);

					affiche.fp.panel_param.add(affiche.fp.champ1);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("Nom du malade :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					
					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_chambre))
				{
					System.out.println("Vous recherchez une chambre");
					
					/**On affiche les param possiblent pour la table Chambre*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("no_chambre");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("surveillant");
					affiche.fp.champ3 = new JCheckBox("nb_lits");
					affiche.fp.champ4 = new JCheckBox("code_service");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					affiche.fp.champ4.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					affiche.fp.panel_param.add(affiche.fp.champ4);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("N\u00ba de chambre :");
					affiche.fp.cle_recherche2 = new JLabel ("Surveillant :");
					affiche.fp.cle_recherche3 = new JLabel("Nombre de lit :");
					affiche.fp.cle_recherche4 = new JLabel("Code du service");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					affiche.fp.text_recherche4 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);	
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);	
					affiche.fp.west2.add(affiche.fp.text_recherche3);
					affiche.fp.west2.add(affiche.fp.cle_recherche4);	
					affiche.fp.west2.add(affiche.fp.text_recherche4);
					
					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_rdv))
				{
					System.out.println("Vous recherchez un rendez-vous");
					
					/**On affiche les param possiblent pour la table RDV*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("Heure");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("Date");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);

					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("Heure :");
					affiche.fp.cle_recherche2 = new JLabel("Date :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					
					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_malade))
				{
					System.out.println("Vous recherchez un malade");
					
					/**On affiche les param possiblent pour la table Malade*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("numero");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("nom");
					affiche.fp.champ3 = new JCheckBox("prenom");
					affiche.fp.champ4 = new JCheckBox("mutuelle");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					affiche.fp.champ4.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					affiche.fp.panel_param.add(affiche.fp.champ4);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("numero :");
					affiche.fp.cle_recherche2 = new JLabel("nom :");
					affiche.fp.cle_recherche3 = new JLabel("prenom :");
					affiche.fp.cle_recherche4 = new JLabel("mutuelle :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					affiche.fp.text_recherche4 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);
					affiche.fp.west2.add(affiche.fp.text_recherche3);
					affiche.fp.west2.add(affiche.fp.cle_recherche4);
					affiche.fp.west2.add(affiche.fp.text_recherche4);
					
					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_infirmier))
				{
					System.out.println("Vous recherchez un infirmier");
					
					/**On affiche les param possiblent pour la table Infirmier*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("numero");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("code_service");
					affiche.fp.champ3 = new JCheckBox("rotation");
					affiche.fp.champ4 = new JCheckBox("salaire");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					affiche.fp.champ4.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					affiche.fp.panel_param.add(affiche.fp.champ4);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("Num\u00e9ro :");
					affiche.fp.cle_recherche2 = new JLabel("Code du service :");
					affiche.fp.cle_recherche3 = new JLabel("Rotation :");
					affiche.fp.cle_recherche4 = new JLabel("Salaire :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					affiche.fp.text_recherche4 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);
					affiche.fp.west2.add(affiche.fp.text_recherche3);
					affiche.fp.west2.add(affiche.fp.cle_recherche4);
					affiche.fp.west2.add(affiche.fp.text_recherche4);

					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_docteur))
				{
					System.out.println("Vous recherchez un docteur");
					
					/**On affiche les param possiblent pour la table Docteur*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("numero");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("specialite");
					affiche.fp.champ3 = new JCheckBox("nom");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					

					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("Num\u00e9ro :");
					affiche.fp.cle_recherche2 = new JLabel("Specialit\u00e9 :");
					affiche.fp.cle_recherche3 = new JLabel("Nom du docteur :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);
					affiche.fp.west2.add(affiche.fp.text_recherche3);

					affiche.fp.revalidate();
					affiche.fp.repaint();
				}
				else if(e.getSource().equals(affiche.fp.btn_soigne))
				{
					System.out.println("Vous recherchez un soign\u00e9");
					
					/**On affiche les param possiblent pour la table Soigne*/
					affiche.fp.panel_param.removeAll();
					affiche.fp.champ1 = new JCheckBox("N\u00ba docteur");
					affiche.fp.champ1.setSelected(true);
					affiche.fp.champ2 = new JCheckBox("N\u00ba malade");
					
					affiche.fp.champ3 = new JCheckBox ("Nom du malade");
					
					affiche.fp.champ1.addItemListener(this);
					affiche.fp.champ2.addItemListener(this);
					affiche.fp.champ3.addItemListener(this);
					
					affiche.fp.panel_param.add(affiche.fp.champ1);
					affiche.fp.panel_param.add(affiche.fp.champ2);
					affiche.fp.panel_param.add(affiche.fp.champ3);
					
					affiche.fp.west2.removeAll();
					
					affiche.fp.cle_recherche1 = new JLabel("N\u00ba docteur :");
					affiche.fp.cle_recherche2 = new JLabel("N\u00ba malade :");
					affiche.fp.cle_recherche3 = new JLabel("Nom du malade :");
					
					affiche.fp.text_recherche1 = new JTextField(10);
					affiche.fp.text_recherche2 = new JTextField(10);
					affiche.fp.text_recherche3 = new JTextField(10);
					
					affiche.fp.west2.add(affiche.fp.cle_recherche1);	
					affiche.fp.west2.add(affiche.fp.text_recherche1);
					affiche.fp.west2.add(affiche.fp.cle_recherche2);
					affiche.fp.west2.add(affiche.fp.text_recherche2);
					affiche.fp.west2.add(affiche.fp.cle_recherche3);
					affiche.fp.west2.add(affiche.fp.text_recherche3);

					affiche.fp.revalidate();
					affiche.fp.repaint();
				}	
				
				else if(e.getSource().equals(affiche.fp.btn_save_rdv))
				{
					System.out.println("RDV enregistr\u00e9");
				}
				
				/**INSERT, DELETE, MODIFY data in database*/
				else if(e.getSource().equals(btn_variable)) {
					
					/**INSERT DATA*/
					if(btn_variable.getText()=="Inserer les nouvelles donnees") {
						
	
						Font font = new Font ("Times", Font.ITALIC, 14);
						String exception=null;
						int user_int=0;
						int user_int2=0;
						int user_int3=0;
						int user_int4=0;
						int user_int5=0;
						
						
						/**get selected radio button*/
						String btn_temp=getSelectedRadioButton();
				
						/**get user entry*/
						String user_text1=text_edition1.getText();
						String user_text2=text_edition2.getText();
						String user_text3=text_edition3.getText();
						String user_text4=text_edition4.getText();
						String user_text5=text_edition5.getText();
						String user_text6=text_edition6.getText();
						
						
						/**Checking JTextField*/
						String tab[]=new String[12];
						tab=instanceOf(panelCenter_Edition);
						
						
						for(int i=1;i<tab.length;i++) {
							
							if(tab[i]!=null) {
								
								if(tab[i].length()==0) {
									
									System.out.println("la case " +i+ " du tableau est vide");
									exception="Erreur, certains champs n'ont pas ete renseignes";
									
								}
	
							}
						}
						
						/**repaint window*/
						writeException.setFont(font);
						writeException.setForeground(Color.RED);
						writeException.setText(exception);
						fenEditionMenu.revalidate();
						fenEditionMenu.repaint();
						
						if(exception==null) {
							
							/**SERVICE*/
							if(btn_temp=="Service") {
								
								/**attention, il faut g�n�rer une nouvelle exception car plusieurs batiments pevent �tre dans un m�me service*/
								/**Ca marche mais l'exception n'est pas la bonne */
								try {
									
									user_int=Integer.parseInt(user_text4);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=service.insertionService(user_text1, user_text2, user_text3, user_int, connexion);
								}
							
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
								
							}
							
							/**CHAMBRE*/
							else if(btn_temp=="Chambre") {
								
								/**attention au exception, exemple : c'est une infirmi�re qui doit etre le surveillant de la chambre*/
								
								try {
									
									user_int2=Integer.parseInt(user_text2);
									user_int3=Integer.parseInt(user_text3);
									user_int4=Integer.parseInt(user_text4);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=chambre.insertionChambre(user_text1, user_int2, user_int3, user_int4, connexion);
								}
								
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
							
							}
							
							/**Soigne*/
							else if(btn_temp=="Soigne") {
								
							
								try {
									
									user_int=Integer.parseInt(user_text1);
									user_int2=Integer.parseInt(user_text2);
									
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=soigne.insertionSoigne(user_int, user_int2, connexion);
								}
								
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
							
							}
							
							
							
							/**EMPLOYES*/
							else if(btn_temp=="Employes") {
								
								try {
									
									user_int=Integer.parseInt(user_text1);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=employe.insertionEmploye(user_int, user_text2, user_text3, user_text4, user_text5, connexion);
								}
							
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
			
							}
							
							
							
							/**DOCTEUR*/
							else if(btn_temp=="Docteur") {
								
								try {
									
									user_int=Integer.parseInt(user_text1);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								
								if(exception==null) {
									
									exception=docteur.insertionDocteur(user_int, user_text2, connexion);
								}
								
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
								
							}
							
						
							/**INFIRMIER*/
							else if(btn_temp=="Infirmier") {
								
								try {
									
									user_int=Integer.parseInt(user_text1);
									user_int4=Integer.parseInt(user_text4);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								
								
								if(exception==null) {
									
									exception=infirmier.insertionInfirmier(user_int,user_text2,user_text3,user_int4,connexion);
								}
								
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
								
							}
							
							
							/**MALADE*/
							else if (btn_temp=="Malade") {
								
								try {
									
									user_int=Integer.parseInt(user_text1);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=malade.insertionMalade(user_int, user_text2, user_text3, user_text4, user_text5, user_text6, connexion);
								}
							
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
								
								
							}
							
							else if(btn_temp=="Hospitalisation") {
								
								try {
									
									user_int=Integer.parseInt(user_text1);
									user_int3=Integer.parseInt(user_text3);
									user_int4=Integer.parseInt(user_text4);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=hospitalisation.insertionHospitalisation(user_int, user_text2, user_int3, user_int4, connexion);
								}
							
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
								
							}
							
							else if (btn_temp=="Rendez-vous") {
								try {
									
									user_int3=Integer.parseInt(user_text3);
									user_int4=Integer.parseInt(user_text4);
									user_int5=Integer.parseInt(user_text5);
									
								}catch(NumberFormatException n) {
									
									exception="Erreur, le format de certaines entr\u00e9es est incorrect ! ";
								}
								
								if(exception==null) {
									
									exception=rdv.insertionRDV(user_text1, user_text2, user_int, user_int4, user_int5, connexion);
								}
							
								/**repaint window*/
								writeException.setFont(font);
								writeException.setText(exception);
								fenEditionMenu.revalidate();
								fenEditionMenu.repaint();
								
							}
							
							
					}
							
						reporting();
						reporting2();
						reporting3();
						reporting4();
						
						
				}
		
					
					if(btn_variable.getText()=="Supprimer les donnees") {
						
						String entry1=text_edition1.getText();
						String entry2=text_edition2.getText();
						String entry3=text_edition3.getText();
						String entry4=text_edition4.getText();
						String entry5=text_edition5.getText();
												
						/**get selected radio button*/
						String btn_temp=getSelectedRadioButton();
						
						String info = "DELETE SUCCESSFUL";
						
						
						/**SERVICE*/
						if(btn_temp=="Service") {
							try {
							service.deleteService(entry1, entry2, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser une clef de suppression");
							}					
						}
						
						/**CHAMBRE*/
						else if(btn_temp=="Chambre") {
							try {
							chambre.deleteChambre(entry1, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser une clef de suppression");
							}
						}										
						
						/**EMPLOYES*/
						else if(btn_temp=="Employes") {
							try {
							employe.deleteEmploye(entry1, entry2, entry3, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser une clef de suppression");
							}
						
						}						
						
						/**MALADE*/
						else if (btn_temp=="Malade") {
							
							try {
							malade.deleteMalade(entry1, entry2, entry3, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch( java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser une clef de suppression");
							}
							
						}
						
						else if(btn_temp=="Hospitalisation") {
							try {
							hospitalisation.deleteHospitalisation(entry1, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser une clef de suppression");
							}
						}
						
						else if (btn_temp=="Rendez-vous") {
							try {
								rdv.deleteRDV(entry1,entry2,entry3,entry4,entry5, connexion);
								javax.swing.JOptionPane.showMessageDialog(null,info);
								fenEditionMenu.dispose();
								}catch(java.lang.IndexOutOfBoundsException e4) {
									javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser tous les champs pour supprimer");
								}
							
						}
						
						reporting();
						reporting2();
						reporting3();
						reporting4();
							
					}
					
					if(btn_variable.getText()=="Enregistrer les modifications") {
						
						String entry1=text_edition1.getText();
						String entry2=text_edition2.getText();
						String entry3=text_edition3.getText();
						String entry4=text_edition4.getText();
						String entry5=text_edition5.getText();
						String entry6=text_edition6.getText();
												
						/**get selected radio button*/
						String btn_temp=getSelectedRadioButton();
						
						String info = "UPDATE SUCCESSFUL";
						
						
						/**SERVICE*/
						if(btn_temp=="Service") {
							try {
							service.updateService(entry1, entry2,entry3,entry4, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser la clef de modification");
							}					
						}
						
						/**CHAMBRE*/
						else if(btn_temp=="Chambre") {
							try {
							chambre.updateChambre(entry1,entry2,entry3, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser la clef de modification");
							}
						}										
						
						/**EMPLOYES*/
						else if(btn_temp=="Employes") {
							try {
							employe.updateEmploye(entry1, entry2, entry3,entry4,entry5, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser la clef de modification");
							}
						
						}						
						
						/**MALADE*/
						else if (btn_temp=="Malade") {
							
							try {
							malade.updateMalade(entry1, entry2, entry3,entry4,entry5,entry6, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch( java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser la clef de modification");
							}
							
						}
						
						else if(btn_temp=="Hospitalisation") {
							try {
							hospitalisation.updateHospitalisation(entry1,entry2,entry3,entry4, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser la clef de modification");
							}
						}
						
						else if (btn_temp=="Rendez-vous") {
							
							
						}
						
						else if(btn_temp=="Infirmier") {
							try {
							infirmier.updateInfirmier(entry1,entry2,entry3, connexion);
							javax.swing.JOptionPane.showMessageDialog(null,info);
							fenEditionMenu.dispose();
							}catch(java.lang.IndexOutOfBoundsException e4) {
								javax.swing.JOptionPane.showMessageDialog(null,"Merci de preciser la clef de modification");
							}
						}
						
						reporting();
						reporting2();
						reporting3();
						reporting4();
							
					}
					
				}
				
				else if(e.getSource().equals(btn_annuler)) {
					
					
					System.out.println("Fermeture de la fenetre edition");
					fenEditionMenu.dispose();
				
				}
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
		
		
		if(affiche.monThread.stopBarre()==100) {
			
			affiche.monThread.beginPage.dispose();
			affiche.newFenetre_choix();
			affiche.ajoutActionListenerChoix();
			
		}

		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	/**edit menu window (insert, update
	 * @param titreFenetre Titre de la fenetre
	 * @param titre_bouton Titre du bouton*/
	public void fenetreMenuEdition(String titreFenetre,String titre_bouton) {
		
		
		JPanel panelSouth_Edition;
		JLabel label_edition1,label_edition2,label_edition3,label_edition4,label_edition5,label_edition6;
		
		
		/**new widow declaration*/
		fenEditionMenu = new JFrame();
		fenEditionMenu.setLayout(new BorderLayout());
		fenEditionMenu.setTitle(titreFenetre);
		//fenEditionMenu.setSize(430, 310);
		fenEditionMenu.setVisible(true);
		
		/**window panel declaration (south and center)*/
		
		/**panel with buttons (cancel, insert, delete, modify)*/
		panelSouth_Edition = new JPanel();
		panelSouth_Edition.setLayout(new BoxLayout(panelSouth_Edition,BoxLayout.LINE_AXIS));
		
		/**panel with user exception (error etc.)*/
		panelSouthException=new JPanel();
		panelSouthException.setLayout(new BoxLayout(panelSouthException,BoxLayout.LINE_AXIS));
		
		/**Panel with all JTextField*/
		panelCenter_Edition = new JPanel();
		panelCenter_Edition.setLayout(new GridLayout(0,2));
		
		/**panel with all others Panels*/
		panelSouthTotal=new JPanel();
		panelSouthTotal.setLayout(new BoxLayout(panelSouthTotal,BoxLayout.PAGE_AXIS));
		
		
		/**Label with exception*/
		writeException=new JLabel();
		
		/**test*/
		//writeException.setText("coucou");
		panelSouthException.add(writeException);
		
		
		/**button frame declaration*/
		btn_variable = new JButton(titre_bouton);
		panelSouth_Edition.add(btn_variable);
		btn_annuler = new JButton("Annuler");
		panelSouth_Edition.add(btn_annuler);
		
		/**add panel */
		panelSouthTotal.add(panelSouth_Edition);
		//panelSouthTotal.add(panelSouthException);
		panelSouthTotal.add(panelSouthException);
		
		
		/**instantiate label and textField*/
		label_edition1=new JLabel();
		label_edition2=new JLabel();
		label_edition3=new JLabel();
		label_edition4=new JLabel();
		label_edition5=new JLabel();
		label_edition6=new JLabel();
		
		text_edition1=new JTextField(20);
		text_edition2=new JTextField(20);
		text_edition3=new JTextField(20);
		text_edition4=new JTextField(20);
		text_edition5=new JTextField(20);
		text_edition6=new JTextField(20);
		
		
		/**test JRadioButton*/
		if(affiche.fp.btn_service.isSelected()) {
			
			fenEditionMenu.setSize(430, 250);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Code du service : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nom du service : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Batiment du service : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Directeur du service : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
				
		}
		
		else if(affiche.fp.btn_chambre.isSelected()) {
			
			fenEditionMenu.setSize(430, 250);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Code service : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Numero : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Surveillant  : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Nombre de lit : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
		}
		
		else if(affiche.fp.btn_employe.isSelected()) {
			
			
			fenEditionMenu.setSize(430, 310);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de l'employe : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nom de l'employe : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Prenom de l'employe : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Telephone de l'employe : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			label_edition5.setText("Adresse de l'employe : ");
			panelCenter_Edition.add(label_edition5);
			panelCenter_Edition.add(text_edition5);
		}
		
		else if(affiche.fp.btn_malade.isSelected()) {
			
			fenEditionMenu.setSize(430, 310);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero du malade : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nom du malade : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Prenom du malade : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Telephone du malade: ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			label_edition5.setText("Adresse du malade: ");
			panelCenter_Edition.add(label_edition5);
			panelCenter_Edition.add(text_edition5);
			
			label_edition6.setText("Mutuelle du malade: ");
			panelCenter_Edition.add(label_edition6);
			panelCenter_Edition.add(text_edition6);
			
		}
		
		else if(affiche.fp.btn_hospitalisation.isSelected()) {
			
			fenEditionMenu.setSize(430, 250);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de malade (cle secondaire) : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Code du service (cle secondaire): ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Numero de chambre : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Lit : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
		
		}
		
		else if(affiche.fp.btn_rdv.isSelected()) {
			
			fenEditionMenu.setSize(430, 250);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Date du rendez-vous: ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Code du service : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Heure du rendez vous: ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Numero employe (cle secondaire) : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			label_edition5.setText("Numero malade (cle secondaire) : ");
			panelCenter_Edition.add(label_edition5);
			panelCenter_Edition.add(text_edition5);
			
			
		}
		
		else if(affiche.fp.btn_docteur.isSelected()) {
			
			fenEditionMenu.setSize(430, 130);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Sp\u009ecialite : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
				
		}
		
		else if(affiche.fp.btn_infirmier.isSelected()) {
			
			fenEditionMenu.setSize(430, 250);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Code service : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Rotation : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Salaire : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
				
		}
		
		
		else if(affiche.fp.btn_soigne.isSelected()) {
			
			fenEditionMenu.setSize(430, 130);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero docteur: ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Numero malade : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
				
		}
		
		
		
		
		
		/**add ActionListener button*/
		btn_variable.addActionListener(this);
		btn_annuler.addActionListener(this);
		
		
		
		/**add in frame*/
		fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
		fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
		//fenEditionMenu.add(ab,BorderLayout.SOUTH);
		
		
		/**centered widow*/
		fenEditionMenu.setLocationRelativeTo(null);
	
	}
	
	/**delete menu window
	 * @param titreFenetre Titre de la fenetre
	 * @param titre_bouton Titre du bouton*/
	public void fenetreMenuSuppression(String titreFenetre,String titre_bouton) {
		
		
		JPanel panelSouth_Edition;
		JLabel label_edition1,label_edition2,label_edition3,label_edition4,label_edition5;
		
		
		/**new widow declaration*/
		fenEditionMenu = new JFrame();
		fenEditionMenu.setLayout(new BorderLayout());
		fenEditionMenu.setTitle(titreFenetre);
		//fenEditionMenu.setSize(430, 150);
		fenEditionMenu.setVisible(true);
		
		/**window panel declaration (south and center)*/
		
		/**panel with buttons (cancel, insert, delete, modify)*/
		panelSouth_Edition = new JPanel();
		panelSouth_Edition.setLayout(new BoxLayout(panelSouth_Edition,BoxLayout.LINE_AXIS));
		
		/**panel with user exception (error etc.)*/
		panelSouthException=new JPanel();
		panelSouthException.setLayout(new BoxLayout(panelSouthException,BoxLayout.LINE_AXIS));
		
		/**Panel with all JTextField*/
		panelCenter_Edition = new JPanel();
		panelCenter_Edition.setLayout(new GridLayout(0,2));
		
		/**panel with all others Panels*/
		panelSouthTotal=new JPanel();
		panelSouthTotal.setLayout(new BoxLayout(panelSouthTotal,BoxLayout.PAGE_AXIS));
		
		
		/**Label with exception*/
		writeException=new JLabel();
		
		/**test*/
		//writeException.setText("coucou");
		panelSouthException.add(writeException);
		
		
		/**button frame declaration*/
		btn_variable = new JButton(titre_bouton);
		panelSouth_Edition.add(btn_variable);
		btn_annuler = new JButton("Annuler");
		panelSouth_Edition.add(btn_annuler);
		
		/**add panel */
		panelSouthTotal.add(panelSouth_Edition);
		//panelSouthTotal.add(panelSouthException);
		panelSouthTotal.add(panelSouthException);
		
		
		/**instantiate label and textField*/
		label_edition1=new JLabel();
		label_edition2=new JLabel();
		label_edition3=new JLabel();
		label_edition4=new JLabel();
		label_edition5=new JLabel();
		
		
		text_edition1=new JTextField(20);
		text_edition2=new JTextField(20);
		text_edition3=new JTextField(20);
		text_edition4=new JTextField(20);
		text_edition5=new JTextField(20);
		
		/**test JRadioButton*/
		if(affiche.fp.btn_service.isSelected()) {
			
			fenEditionMenu.setSize(430, 135);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Code du service : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			
			label_edition2.setText("Directeur du service : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
				
		}
		
		else if(affiche.fp.btn_chambre.isSelected()) {
			
			fenEditionMenu.setSize(430, 104);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de Chambre : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
						
		}
		
		else if(affiche.fp.btn_employe.isSelected()) {
			
			fenEditionMenu.setSize(430, 170);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de l'employe : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nom de l'employe : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Prenom de l'employe : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
			
		}
		
		else if(affiche.fp.btn_malade.isSelected()) {
			
			fenEditionMenu.setSize(430, 170);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero du malade : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nom du malade : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Prenom du malade : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);		
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
		}
		
		else if(affiche.fp.btn_hospitalisation.isSelected()) {
			
			fenEditionMenu.setSize(430, 135);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de malade : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);

			
			label_edition2.setText("Numero de chambre : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
		
		}
		
		else if(affiche.fp.btn_rdv.isSelected()) {
			
			fenEditionMenu.setSize(430, 200);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Heure du rendez-vous : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Date du rendez-vous : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Code service  : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Numero employe : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			label_edition5.setText("Numero malade : ");
			panelCenter_Edition.add(label_edition5);
			panelCenter_Edition.add(text_edition5);
			
			
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
			
		}
		else if(affiche.fp.btn_soigne.isSelected()) {
			
			String info = "Vous devez supprimer le Docteur ou le Malade associe";
			javax.swing.JOptionPane.showMessageDialog(null,info);	
		}
		else if(affiche.fp.btn_docteur.isSelected()) {
			
			String info = "Vous devez supprimer le docteur via l'onglet employe";
			javax.swing.JOptionPane.showMessageDialog(null,info);	
		}
		else if(affiche.fp.btn_infirmier.isSelected()) {
	
			String info = "Vous devez supprimer l'infirmier via l'onglet employe";
			javax.swing.JOptionPane.showMessageDialog(null,info);	
		}
		
		/**add ActionListener button*/
		btn_variable.addActionListener(this);
		btn_annuler.addActionListener(this);
		
		
		
		
	
	}
	
	/**update menu window
	 * @param titreFenetre Titre de la fenetre
	 * @param titre_bouton Titre du bouton*/
	public void fenetreMenuModification(String titreFenetre,String titre_bouton) {
		
		
		JPanel panelSouth_Edition;
		JLabel label_edition1,label_edition2,label_edition3,label_edition4,label_edition5,label_edition6;
		
		
		/**new widow declaration*/
		fenEditionMenu = new JFrame();
		fenEditionMenu.setLayout(new BorderLayout());
		fenEditionMenu.setTitle(titreFenetre);
		//fenEditionMenu.setSize(430, 150);
		fenEditionMenu.setVisible(true);
		
		/**window panel declaration (south and center)*/
		
		/**panel with buttons (cancel, insert, delete, modify)*/
		panelSouth_Edition = new JPanel();
		panelSouth_Edition.setLayout(new BoxLayout(panelSouth_Edition,BoxLayout.LINE_AXIS));
		
		/**panel with user exception (error etc.)*/
		panelSouthException=new JPanel();
		panelSouthException.setLayout(new BoxLayout(panelSouthException,BoxLayout.LINE_AXIS));
		
		/**Panel with all JTextField*/
		panelCenter_Edition = new JPanel();
		panelCenter_Edition.setLayout(new GridLayout(0,2));
		
		/**panel with all others Panels*/
		panelSouthTotal=new JPanel();
		panelSouthTotal.setLayout(new BoxLayout(panelSouthTotal,BoxLayout.PAGE_AXIS));
		
		
		/**Label with exception*/
		writeException=new JLabel();
		
		/**test*/
		//writeException.setText("coucou");
		panelSouthException.add(writeException);
		
		
		/**button frame declaration*/
		btn_variable = new JButton(titre_bouton);
		panelSouth_Edition.add(btn_variable);
		btn_annuler = new JButton("Annuler");
		panelSouth_Edition.add(btn_annuler);
		
		/**add panel */
		panelSouthTotal.add(panelSouth_Edition);
		//panelSouthTotal.add(panelSouthException);
		panelSouthTotal.add(panelSouthException);
		
		
		/**instantiate label and textField*/
		label_edition1=new JLabel();
		label_edition2=new JLabel();
		label_edition3=new JLabel();
		label_edition4=new JLabel();
		label_edition5=new JLabel();
		label_edition6=new JLabel();
		
		
		text_edition1=new JTextField(20);
		text_edition2=new JTextField(20);
		text_edition3=new JTextField(20);
		text_edition4=new JTextField(20);
		text_edition5=new JTextField(20);
		text_edition6=new JTextField(20);
		
		
		/**test JRadioButton*/
		if(affiche.fp.btn_service.isSelected()) {
			
			fenEditionMenu.setSize(430, 135);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Code du service a modifier : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			
			label_edition2.setText("Nouveau nom du service : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Nouveau batiment du service : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			
			label_edition4.setText("Nouveau directeur du service : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
				
		}
		
		else if(affiche.fp.btn_chambre.isSelected()) {
			
			fenEditionMenu.setSize(430, 104);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de Chambre aux informations a modifier : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition1.setText("Nouveau surveillant : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition1.setText("Nouveau nombre de lit : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
						
		}
		
		else if(affiche.fp.btn_employe.isSelected()) {
			
			fenEditionMenu.setSize(430, 170);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de l'employe aux informations a modifier : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nouveau nom de l'employe : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Nouveau prenom de l'employe : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Nouvelle adresse de l'employe : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			label_edition5.setText("Nouveau telephone de l'employe : ");
			panelCenter_Edition.add(label_edition5);
			panelCenter_Edition.add(text_edition5);
			
			
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
			
		}
		
		else if(affiche.fp.btn_malade.isSelected()) {
			
			fenEditionMenu.setSize(430, 170);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero du malade aux informations a modifier : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nouveau nom du malade : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Nouveau prenom du malade : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);	
			
			label_edition4.setText("Nouvelle adresse du malade : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			label_edition5.setText("Nouveau telephone du malade : ");
			panelCenter_Edition.add(label_edition5);
			panelCenter_Edition.add(text_edition5);
			
			label_edition6.setText("Nouvelle mutuelle du malade : ");
			panelCenter_Edition.add(label_edition6);
			panelCenter_Edition.add(text_edition6);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
		}
		
		else if(affiche.fp.btn_hospitalisation.isSelected()) {
			
			fenEditionMenu.setSize(430, 135);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero du malade hospitalise : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);

			
			label_edition2.setText("Nouveau code de service : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Nouveau numero de chambre: ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);

			
			label_edition4.setText("Nouveau numero de lit : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
		
		}
		
		else if(affiche.fp.btn_rdv.isSelected()) {
			
			fenEditionMenu.setSize(430, 200);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Heure du rendez-vous : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Date du rendez-vous : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Code service  : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			label_edition4.setText("Numero employe : ");
			panelCenter_Edition.add(label_edition4);
			panelCenter_Edition.add(text_edition4);
			
			
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
			
		}
		else if(affiche.fp.btn_soigne.isSelected()) {
			
			String info = "Vous ne pouvez pas modifie un couple Docteur/Patient";
			javax.swing.JOptionPane.showMessageDialog(null,info);	
		}
		else if(affiche.fp.btn_docteur.isSelected()) {
			
			String info = "Un docteur ne peut pas changer de specialite !!!!!";
			javax.swing.JOptionPane.showMessageDialog(null,info);	
		}
		else if(affiche.fp.btn_infirmier.isSelected()) {
			
			fenEditionMenu.setSize(430, 200);
			
			/**Creating Label and TextField*/
			label_edition1.setText("Numero de l'infirmier aux informations a modifier : ");
			panelCenter_Edition.add(label_edition1);
			panelCenter_Edition.add(text_edition1);
			
			label_edition2.setText("Nouvelle rotation : ");
			panelCenter_Edition.add(label_edition2);
			panelCenter_Edition.add(text_edition2);
			
			label_edition3.setText("Nouveau salaire  : ");
			panelCenter_Edition.add(label_edition3);
			panelCenter_Edition.add(text_edition3);
			
			/**add in frame*/
			fenEditionMenu.add(panelCenter_Edition,BorderLayout.CENTER);
			fenEditionMenu.add(panelSouthTotal,BorderLayout.SOUTH);
			//fenEditionMenu.add(ab,BorderLayout.SOUTH);
			
			
			/**centered widow*/
			fenEditionMenu.setLocationRelativeTo(null);
				
		}
		
		/**add ActionListener button*/
		btn_variable.addActionListener(this);
		btn_annuler.addActionListener(this);
	}
	
	/**get selected radio button in order to further execute query on the related table
	 * @return String Texte du testButton*/
	public String getSelectedRadioButton() {
		
		JRadioButton testButton = null;
		Enumeration<AbstractButton> researchRadioButton=affiche.fp.groupe_recherche.getElements();  
        while(researchRadioButton.hasMoreElements())  
        {  
           testButton=(JRadioButton)researchRadioButton.nextElement();  
           
           if(testButton.isSelected())  
           {    
        	   break;
           }  
        }
        return testButton.getText();
	}
	
	
	public String [] instanceOf(JComponent component) {
		
		String monTest[] = new String [12];
		
		for (int i=0;i<component.getComponentCount();i++) {
		
			if(component.getComponent(i) instanceof JTextField) {
				
				monTest[i] = ((JTextField)component.getComponent(i)).getText();		
			}
					
		}
		return monTest;
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		if(e.getStateChange() == ItemEvent.SELECTED)
		{
			System.out.println("Selection champ1");
			if(affiche.fp.champ1.getText().equals("Num\u00e9ro"))
			{
				affiche.fp.cle_recherche1.setText("Num\u00e9ro :");
			}
			else if(affiche.fp.champ1.getText().equals("Code"))
			{
				affiche.fp.cle_recherche1.setText("Code :");
			}
			else if(affiche.fp.champ1.getText().equals("N\u00ba de chambre"))
			{
				affiche.fp.cle_recherche1.setText("N\u00ba de chambre :");
			}
			else if(affiche.fp.champ1.getText().equals("N\u00ba de malade"))
			{
				affiche.fp.cle_recherche1.setText("N\u00ba de malade");
			}
		}
	}
	
	
	public void rechercheMalade()
	{
		/**Entete du tableau*/
		String[] columnNames = {"Numero", "Nom", "Prenom", "Adresse", "Telephone", "Mutuelle"};  
		
		/**Liste qui recoit le contenu de l'arraylist*/
		String [][] liste = null;
		JTable table = null;

		
		ArrayList<Vector<String>> list = new ArrayList<Vector<String>>();
			
		
		/**Checking JTextField*/
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
			
			
		if(affiche.fp.champ1.isSelected()) {
				
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[1].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier un numero", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=malade.rechercheMalade(affiche.fp.champ1, affiche.fp.text_recherche1, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
					
			}	
		}
		
		
		else if(affiche.fp.champ2.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[3].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier un nom", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=malade.rechercheMalade(affiche.fp.champ2, affiche.fp.text_recherche2, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
		}
		
		
		else if(affiche.fp.champ3.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[5].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifie un prenom", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=malade.rechercheMalade(affiche.fp.champ3, affiche.fp.text_recherche3, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
		}
		
		
		else if(affiche.fp.champ4.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[7].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier une mutuelle", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=malade.rechercheMalade(affiche.fp.champ4, affiche.fp.text_recherche4, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
		}
			


		/**On declare un tab de string a la bonne dimension, c'est a dire 6 car il y a 6 champs pour un malade*/
		liste = new String[list.size()][6];
			
		for(int i=0; i<list.size(); i++)
		{
			for(int j=0; j<list.get(i).size(); j++)
			{
				/**On enregistre les donnees dans le tableau de string*/
				liste[i][j] = list.get(i).get(j).toString();
				System.out.println(liste[i][j]);
			}
		}
			
		
		/**on remove les donnees dans l'ancien tableau*/
		affiche.fp.panel_donnee.removeAll();
							
		/**On cree le tableau a afficher*/
		table = new JTable(liste, columnNames);
		
		/**On l'affiche*/
		affiche.fp.panel_donnee.add(table.getTableHeader(), BorderLayout.NORTH);
	    affiche.fp.panel_donnee.add(new JScrollPane(table), BorderLayout.CENTER);
	    
	    
	    /**On rafraichit*/
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
	   
	}
	
	public void rechercheChambre() {
		
		/**Entete du tableau*/
		String[] columnNames = {"Code du service","Numero de chambre", "Surveillant", "Nombre de lit"};  
		
		/**Liste qui recoit le contenu de l'arraylist*/
		String [][] liste = null;
		JTable table = null;

		
		ArrayList<Vector<String>> list = new ArrayList<Vector<String>>();
			
		
		/**Checking JTextField*/
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
			
			
		if(affiche.fp.champ1.isSelected()) {
				
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[1].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier un numero de chambre", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=chambre.rechercheChambre(affiche.fp.champ1, affiche.fp.text_recherche1, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
					
			}	
		}
		
		
		else if(affiche.fp.champ2.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[3].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier un surveillant", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=chambre.rechercheChambre(affiche.fp.champ2, affiche.fp.text_recherche2, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
		}
		
		
		else if(affiche.fp.champ3.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[5].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifie un nombre de lit", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=chambre.rechercheChambre(affiche.fp.champ3, affiche.fp.text_recherche3, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
		}
		
		else if(affiche.fp.champ4.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[7].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifie le code d'un service", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				list=chambre.rechercheChambre(affiche.fp.champ4, affiche.fp.text_recherche4, connexion);
				if(list.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
		}
		

		/**On declare un tab de string a la bonne dimension, c'est a dire 4 car il y a 4 champs pour une chambre*/
		liste = new String[list.size()][4];
			
		for(int i=0; i<list.size(); i++)
		{
			for(int j=0; j<list.get(i).size(); j++)
			{
				/**On enregistre les donnees dans le tableau de string*/
				liste[i][j] = list.get(i).get(j).toString();
				System.out.println(liste[i][j]);
			}
		}
			
		
		/**on remove les donnees dans l'ancien tableau*/
		affiche.fp.panel_donnee.removeAll();
							
		/**On cree le tableau a afficher*/
		table = new JTable(liste, columnNames);
		
		/**On l'affiche*/
		affiche.fp.panel_donnee.add(table.getTableHeader(), BorderLayout.NORTH);
	    affiche.fp.panel_donnee.add(new JScrollPane(table), BorderLayout.CENTER);
	    
	    
	    /**On rafraichit*/
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
	   
	}
	
	public void rechercheEmploye() {
		
		/**Entete du tableau des employes*/
		String[] columnNames = {"Numero","Nom", "Prenom", "Adresse","Telephone"};  
		

		/**Liste qui recoit le contenu de l'arraylist des employes*/
		String [][] listeEmployes = null;
		
		/**Liste qui recoit le contenu de l'arraylist des docteurs ou des infirmiers*/
		String [][] listeDI = null;
		
		
		/**on declare deux JTable*/
		JTable tableEmploye = null;
		JTable tableEtendue = null;

		/**Arraylist qui recupere la requete de recherche d'un employe*/
		ArrayList<Vector<String>> listEmploye = new ArrayList<Vector<String>>();
		
		/**Arraylist qui recupere la requete de recherche d'un docteur ou d'un infirmier*/
		ArrayList<Vector<String>> listEtendue = new ArrayList<Vector<String>>();
		
		
		/**Checking JTextField*/
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
		
		
		if(affiche.fp.champ1.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[1].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier un numero d'employe", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listEmploye=employe.rechercheEmploye(affiche.fp.champ1, affiche.fp.text_recherche1, connexion);
				if(listEmploye.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
					
			}	
		}
		
		if(affiche.fp.champ2.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[3].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le nom d'un employe", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listEmploye=employe.rechercheEmploye(affiche.fp.champ2, affiche.fp.text_recherche2, connexion);
				if(listEmploye.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
					
			}	
		}
		
		if(affiche.fp.champ3.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[5].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le prenom d'un employe", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listEmploye=employe.rechercheEmploye(affiche.fp.champ3, affiche.fp.text_recherche3, connexion);
				if(listEmploye.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
					
			}	
		}
		
		
		
		/**on appelle le sous programme de recherche des docteurs et/ou des infirmiers*/
		if(listEmploye.size()!=0) {
			
			listEtendue=employe.rechercheEmployeEtendue(listEmploye, connexion);
			System.out.println("la liste etendue a ete instanciee");
			System.out.println("Voici la liste etendue : "+listEtendue);
		
		}
			
		
		/**On declare un tab de string a la bonne dimension, c'est a dire 5 car il y a 5 champs pour une chambre*/
		listeEmployes = new String[listEmploye.size()][5];
			
		for(int i=0; i<listEmploye.size(); i++)
		{
			for(int j=0; j<listEmploye.get(i).size(); j++)
			{
				/**On enregistre les donnees dans le tableau de string*/
				listeEmployes[i][j] = listEmploye.get(i).get(j).toString();
				System.out.println(listeEmployes[i][j]);
			}
		}
		
		
		/**on remove les donnees dans l'ancien tableau*/
		affiche.fp.panel_donnee.removeAll();
		
		
		/**on regarde la taille du deuxieme tableau retourne pour savoir si c'est docteur ou infirmier*/
		/**docteur*/
		
		
		if(listEmploye.size()!=0) {
			
			if(listEtendue.get(0).size()==2) {
				
				System.out.println("test si liste etendue vaut 2");
				
				
				String [] columnNames2 = {"Numero","Specialite"}; 
				
				listeDI = new String[listEtendue.size()][2];
				
				for(int i=0; i<listEtendue.size(); i++)
				{
					for(int j=0; j<listEtendue.get(i).size(); j++)
					{
						
						listeDI[i][j] = listEtendue.get(i).get(j).toString();
						System.out.println(listeDI[i][j]);
					}
				}
				
				tableEtendue = new JTable(listeDI, columnNames2);
				affiche.fp.panel_donnee.add(tableEtendue.getTableHeader(),BorderLayout.CENTER);
			    affiche.fp.panel_donnee.add(new JScrollPane(tableEtendue),BorderLayout.CENTER);
				
			}
			
			
		
			else if(listEtendue.get(0).size()==4) {
				
				System.out.println("test si liste etendue vaut 4");
				
				
				String [] columnNames2 = {"Numero","Code_service","Rotation","Salaire"}; 
				
				listeDI = new String[listEtendue.size()][4];
				
				for(int i=0; i<listEtendue.size(); i++)
				{
					for(int j=0; j<listEtendue.get(i).size(); j++)
					{
						
						listeDI[0][0] = listEtendue.get(0).get(0).toString();
						if(listEtendue.get(0).get(1)!=null) listeDI[0][1] = listEtendue.get(0).get(1).toString();
						listeDI[0][2] = listEtendue.get(0).get(2).toString();
						listeDI[0][3] = listEtendue.get(0).get(3).toString();
					
						//System.out.println(listeDI[i][j]);
					}
				}
				
				tableEtendue = new JTable(listeDI, columnNames2);
				affiche.fp.panel_donnee.add(tableEtendue.getTableHeader(),BorderLayout.CENTER);
			    affiche.fp.panel_donnee.add(new JScrollPane(tableEtendue),BorderLayout.CENTER);
				
			}
			
		}
		

				
		/**On cree le tableau a afficher*/
		tableEmploye = new JTable(listeEmployes, columnNames);
		
		
		/**On l'affiche*/
		affiche.fp.panel_donnee.add(tableEmploye.getTableHeader(),BorderLayout.NORTH);
	    affiche.fp.panel_donnee.add(new JScrollPane(tableEmploye),BorderLayout.NORTH);
	    

	    /**On rafraichit*/
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
		
		
		
	}
	
	public void rechercheService() {
		
		ArrayList<Vector<String>> listService1 = new ArrayList<Vector<String>>();
		String[][] listeService;
		JTable tableService1;
		
		
		ArrayList<Vector<String>> listService2 = new ArrayList<Vector<String>>();
		String[][] listeService2;
		JTable tableService2;
		
		
		
		/**on remove tout de l'ancien panneau*/
		affiche.fp.panel_donnee.removeAll();
		
		listService1=service.rechercheService(connexion);
		
		String [] columnNames = {"Nom du service","Batiment du service","Nom de son directeur","Prenom de son directeur","Specialite de son directeur"};
		
		
		listeService = new String[listService1.size()][5];
			
		for(int i=0; i<listService1.size(); i++)
		{
			for(int j=0; j<listService1.get(i).size(); j++)
			{
				/**On enregistre les donnees dans le tableau de string*/
				listeService[i][j] = listService1.get(i).get(j).toString();
				
			}
		}
		
		
		/**recherche normale*/
		
		/**Checking JTextField*/
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
			
			
		if(affiche.fp.champ1.isSelected()) {
				
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[1].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le code d'un service", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listService2=service.rechercheService2(affiche.fp.champ1, affiche.fp.text_recherche1, connexion);
				if(listService2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}	
		}
		
		
		if(affiche.fp.champ2.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[3].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le nom d'un service", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listService2=service.rechercheService2(affiche.fp.champ2, affiche.fp.text_recherche2, connexion);
				if(listService2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}	
		}
		
		if(affiche.fp.champ3.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[5].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le batiment d'un service", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listService2=service.rechercheService2(affiche.fp.champ3, affiche.fp.text_recherche3, connexion);
				if(listService2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}	
		}
		
		if(affiche.fp.champ4.isSelected()) {
			
			/**on verifie que le JTextField correspondant soit bien rempli*/
			if(tab[7].length()==0) {
					
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le directeur d'un service", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
					
			}
				
			else {
					
				listService2=service.rechercheService2(affiche.fp.champ4, affiche.fp.text_recherche4, connexion);
				if(listService2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}	
		}
		
		/**On declare un tab de string a la bonne dimension, c'est a dire 6 car il y a 6 champs pour un malade*/
		listeService2 = new String[listService2.size()][4];
		
		String [] columnNames2 = {"Code","Nom","Batiment","Directeur"};
			
		for(int i=0; i<listService2.size(); i++)
		{
			for(int j=0; j<listService2.get(i).size(); j++)
			{
				/**On enregistre les donnees dans le tableau de string*/
				listeService2[i][j] = listService2.get(i).get(j).toString();
				
			}
		}
		
			
		/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
		tableService2 = new JTable(listeService2, columnNames2);
		affiche.fp.panel_donnee.add(tableService2.getTableHeader(),BorderLayout.NORTH);
	    affiche.fp.panel_donnee.add(new JScrollPane(tableService2),BorderLayout.NORTH);
	    
	    
		if(listService2.size()!=0) {
		
			/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
			tableService1 = new JTable(listeService, columnNames);
			affiche.fp.panel_donnee.add(tableService1.getTableHeader(),BorderLayout.CENTER);
		    affiche.fp.panel_donnee.add(new JScrollPane(tableService1),BorderLayout.CENTER);
			
		}
			
	    /**On rafraichit*/
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();

	}
	
	public void rechercheHospitalisation() {
		

		ArrayList<Vector<String>> listHospitalisation1 = new ArrayList<Vector<String>>();
		String[][] listeHospitalisation1;
		JTable tableHospitalisation;
		/**Entete du tableau*/
		String[] columnNames = {"Code du service","Numero de chambre", "lit", "Nom","Prenom","Adresse","Mutuelle"};
		

		/**on remove tout de l'ancien panneau*/
		affiche.fp.panel_donnee.removeAll();
		
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
		
		if(affiche.fp.champ1.isSelected()) {
			
			if(tab[1].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le nom d'un malade", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
				
			}
			
			else {
				
				listHospitalisation1=hospitalisation.rechercheHospitalisation(affiche.fp.champ1,affiche.fp.text_recherche1,connexion);
				if(listHospitalisation1.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
			}
			
		}
		
		
		listeHospitalisation1 = new String[listHospitalisation1.size()][7];
			
		for(int i=0; i<listHospitalisation1.size(); i++)
		{
			for(int j=0; j<listHospitalisation1.get(i).size(); j++)
			{
				
				listeHospitalisation1[i][j] = listHospitalisation1.get(i).get(j).toString();
				
			}
		}
		
		/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
		tableHospitalisation = new JTable(listeHospitalisation1, columnNames);
		affiche.fp.panel_donnee.add(tableHospitalisation.getTableHeader(),BorderLayout.NORTH);
	    affiche.fp.panel_donnee.add(new JScrollPane(tableHospitalisation),BorderLayout.NORTH);
	    
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
	    
	    
	}
	
	public void rechercheSoigne() {
		
		ArrayList<Vector<String>> listSoigne1 = new ArrayList<Vector<String>>();
		String[][] listeSoigne1;
		JTable tableSoigne;
		
		ArrayList<Vector<String>> listSoigne2 = new ArrayList<Vector<String>>();
		String[][] listeSoigne2;
		JTable tableSoigne2;
		
		ArrayList<Vector<String>> listSoigne3 = new ArrayList<Vector<String>>();
		String[][] listeSoigne3;
		JTable tableSoigne3;
		
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
		
		/**on remove tout de l'ancien panneau*/
		affiche.fp.panel_donnee.removeAll();
		
		if(affiche.fp.champ1.isSelected()) {
			
			if(tab[1].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le numero d'un docteur", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listSoigne1=soigne.rechercheSoigne(affiche.fp.champ1,affiche.fp.text_recherche1,connexion);
				if(listSoigne1.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				else {
					
					String[] columnNames = {"Numero docteur","numero malade", "nom malade", "prenom malade","adresse malade","Mutuelle malade"};
					listeSoigne1 = new String[listSoigne1.size()][6];
					
					for(int i=0; i<listSoigne1.size(); i++)
					{
						for(int j=0; j<listSoigne1.get(i).size(); j++)
						{
							
							listeSoigne1[i][j] = listSoigne1.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableSoigne = new JTable(listeSoigne1, columnNames);
					affiche.fp.panel_donnee.add(tableSoigne.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableSoigne),BorderLayout.NORTH);

					
				}
				
			}
				
		}
		
		if(affiche.fp.champ2.isSelected()) {
			
			if(tab[3].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le numero d'un malade", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			
			else {
				
				listSoigne2=soigne.rechercheSoigne2(affiche.fp.champ2,affiche.fp.text_recherche2,connexion);
				if(listSoigne2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				else {
					
					String[] columnNames = {"Numero docteur","nom docteur", "prenom du docteur","numero malade","nom du malade","Prenom du malade","Adresse du malade","Mutuelle malade"};
					listeSoigne2 = new String[listSoigne2.size()][8];
					
					for(int i=0; i<listSoigne2.size(); i++)
					{
						for(int j=0; j<listSoigne2.get(i).size(); j++)
						{
							
							listeSoigne2[i][j] = listSoigne2.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableSoigne2 = new JTable(listeSoigne2, columnNames);
					affiche.fp.panel_donnee.add(tableSoigne2.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableSoigne2),BorderLayout.NORTH);

					
				}
				
			}
		
			
		}
		
		if(affiche.fp.champ3.isSelected()) {
			
			if(tab[5].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le nom d'un malade", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listSoigne3=soigne.rechercheSoigne3(affiche.fp.champ3,affiche.fp.text_recherche3,connexion);
				if(listSoigne3.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				
				else {
					
					String[] columnNames = {"Numero docteur","nom docteur", "prenom du docteur","numero malade","nom du malade","Prenom du malade","Adresse du malade","Mutuelle malade"};
					listeSoigne3 = new String[listSoigne3.size()][8];
					
					for(int i=0; i<listSoigne3.size(); i++)
					{
						for(int j=0; j<listSoigne3.get(i).size(); j++)
						{
							
							listeSoigne3[i][j] = listSoigne3.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableSoigne3 = new JTable(listeSoigne3, columnNames);
					affiche.fp.panel_donnee.add(tableSoigne3.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableSoigne3),BorderLayout.NORTH);
					
				}
			}
			
		}

	    
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
	    
	}
	
	public void rechercheInfirmier() {
		
		ArrayList<Vector<String>> listInfirmier1 = new ArrayList<Vector<String>>();
		String[][] listeInfirmier1;
		JTable tableInfirmier1;
		
		ArrayList<Vector<String>> listInfirmier2 = new ArrayList<Vector<String>>();
		String[][] listeInfirmier2;
		JTable tableInfirmier2;
		
		ArrayList<Vector<String>> listInfirmier3 = new ArrayList<Vector<String>>();
		String[][] listeInfirmier3;
		JTable tableInfirmier3;
		
		ArrayList<Vector<String>> listInfirmier4 = new ArrayList<Vector<String>>();
		String[][] listeInfirmier4;
		JTable tableInfirmier4;
		
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
		
		/**on remove tout de l'ancien panneau*/
		affiche.fp.panel_donnee.removeAll();
		
		
		if(affiche.fp.champ1.isSelected()) {
			
			if(tab[1].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le numero d'un(e) infirmier(e)", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				listInfirmier1=infirmier.rechercheInfirmier(affiche.fp.champ1,affiche.fp.text_recherche1,connexion);
				System.out.println(listInfirmier1);
				if(listInfirmier1.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				else {
					
					
					String[] columnNames = {"Numero infirmier","nom infirmier", "prenom infirmier","code_service","Rotation","Salaire"};
					listeInfirmier1 = new String[listInfirmier1.size()][6];
					
					for(int i=0; i<listInfirmier1.size(); i++)
					{
						for(int j=0; j<listInfirmier1.get(i).size(); j++)
						{
							
							listeInfirmier1[0][0] = listInfirmier1.get(0).get(0).toString();
							listeInfirmier1[0][1] = listInfirmier1.get(0).get(1).toString();
							listeInfirmier1[0][2] = listInfirmier1.get(0).get(2).toString();
							if(listInfirmier1.get(0).get(3)!=null) listeInfirmier1[0][3] = listInfirmier1.get(0).get(3).toString();
							listeInfirmier1[0][4] = listInfirmier1.get(0).get(4).toString();
							listeInfirmier1[0][5] = listInfirmier1.get(0).get(5).toString();
							//System.out.println(listeInfirmier1[i][j]);
							
						}
					}
					
					
					tableInfirmier1 = new JTable(listeInfirmier1, columnNames);
					affiche.fp.panel_donnee.add(tableInfirmier1.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableInfirmier1),BorderLayout.NORTH);
					

				}
				
			}
				
		}
		
		else if(affiche.fp.champ2.isSelected()) {
			
			
			if(tab[3].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le code du service d'un(e) infirmier(e)", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listInfirmier2=infirmier.rechercheInfirmier2(affiche.fp.champ2,affiche.fp.text_recherche2,connexion);
				if(listInfirmier2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				else {
					
					String[] columnNames = {"Numero infirmier","nom infirmier", "prenom infirmier","code_service","Rotation","Salaire"};
					listeInfirmier2 = new String[listInfirmier2.size()][6];
					
					for(int i=0; i<listInfirmier2.size(); i++)
					{
						for(int j=0; j<listInfirmier2.get(i).size(); j++)
						{
							
							listeInfirmier2[i][j] = listInfirmier2.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableInfirmier2 = new JTable(listeInfirmier2, columnNames);
					affiche.fp.panel_donnee.add(tableInfirmier2.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableInfirmier2),BorderLayout.NORTH);
					
					
				}
				
			}
			

		}
		
		else if(affiche.fp.champ3.isSelected()) {
			
			
			if(tab[5].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier la rotation d'un(e) infirmier(e)", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listInfirmier3=infirmier.rechercheInfirmier3(affiche.fp.champ3,affiche.fp.text_recherche3,connexion);
				if(listInfirmier3.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				else {
					
					String[] columnNames = {"Numero infirmier","nom infirmier", "prenom infirmier","code_service","Rotation","Salaire"};
					listeInfirmier3 = new String[listInfirmier3.size()][6];
					
					for(int i=0; i<listInfirmier3.size(); i++)
					{
						for(int j=0; j<listInfirmier3.get(i).size(); j++)
						{
						
							if(listInfirmier3.get(i).get(3)==null) {
								
								listeInfirmier3[i][0] = listInfirmier3.get(i).get(0).toString();
								listeInfirmier3[i][1] = listInfirmier3.get(i).get(1).toString();
								listeInfirmier3[i][2] = listInfirmier3.get(i).get(2).toString();
								listeInfirmier3[i][4] = listInfirmier3.get(i).get(4).toString();
								listeInfirmier3[i][5] = listInfirmier3.get(i).get(5).toString();
									
							}
								
							else listeInfirmier3[i][j] = listInfirmier3.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableInfirmier3 = new JTable(listeInfirmier3, columnNames);
					affiche.fp.panel_donnee.add(tableInfirmier3.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableInfirmier3),BorderLayout.NORTH);
					
					
				}
				
			}
			
		}
		
	
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
	}
	
	
	public void rechercheDocteur() {
		
		ArrayList<Vector<String>> listDocteur1 = new ArrayList<Vector<String>>();
		String[][] listeDocteur1;
		JTable tableDocteur1;
		
		ArrayList<Vector<String>> listDocteur2 = new ArrayList<Vector<String>>();
		String[][] listeDocteur2;
		JTable tableDocteur2;
		
		ArrayList<Vector<String>> listDocteur3 = new ArrayList<Vector<String>>();
		String[][] listeDocteur3;
		JTable tableDocteur3;
		
		String tab[]=new String[8];
		tab=instanceOf(affiche.fp.west2);
		
		/**on remove tout de l'ancien panneau*/
		affiche.fp.panel_donnee.removeAll();
		
		
		
		
		if(affiche.fp.champ3.isSelected()) {
			
			
			if(tab[5].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le nom d'un docteur", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listDocteur1=docteur.rechercheDocteur(affiche.fp.champ3,affiche.fp.text_recherche3,connexion);
				if(listDocteur1.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				else {
					
					String[] columnNames = {"Numero docteur","Nom docteur", "Prenom du docteur","Specialite"};
					listeDocteur1 = new String[listDocteur1.size()][4];
					
					for(int i=0; i<listDocteur1.size(); i++)
					{
						for(int j=0; j<listDocteur1.get(i).size(); j++)
						{
						
							listeDocteur1[i][j] = listDocteur1.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableDocteur1 = new JTable(listeDocteur1, columnNames);
					affiche.fp.panel_donnee.add(tableDocteur1.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableDocteur1),BorderLayout.NORTH);
					
					
				}
				
			}
			
		}
		
		else if(affiche.fp.champ1.isSelected()) {
			
			
			if(tab[1].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier le numero d'un docteur", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listDocteur2=docteur.rechercheDocteur2(affiche.fp.champ1,affiche.fp.text_recherche1,connexion);
				if(listDocteur2.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				else {
					
					String[] columnNames = {"Numero docteur","Nom docteur", "Prenom du docteur","Specialite"};
					listeDocteur2 = new String[listDocteur2.size()][4];
					
					for(int i=0; i<listDocteur2.size(); i++)
					{
						for(int j=0; j<listDocteur2.get(i).size(); j++)
						{
						
							listeDocteur2[i][j] = listDocteur2.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableDocteur2 = new JTable(listeDocteur2, columnNames);
					affiche.fp.panel_donnee.add(tableDocteur2.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableDocteur2),BorderLayout.NORTH);
					
					
				}
				
			}
			
		}
		
		else if(affiche.fp.champ2.isSelected()) {
			
			
			if(tab[3].length()==0) {
				
				System.out.println("Erreur, vous n'avez pas saisi de cle de recherche");
				JOptionPane.showMessageDialog(null, "Veuillez specifier la specialite d'un docteur", "Recherche impossible",JOptionPane.INFORMATION_MESSAGE);
			}
			
			else {
				
				listDocteur3=docteur.rechercheDocteur3(affiche.fp.champ2,affiche.fp.text_recherche2,connexion);
				if(listDocteur3.size()==0) JOptionPane.showMessageDialog(null, "Aucune correpsondance n'a pu �tre trouvee", "Pas de resultat",JOptionPane.INFORMATION_MESSAGE);
				
				else {
					
					String[] columnNames = {"Numero docteur","Nom docteur", "Prenom du docteur","Specialite"};
					listeDocteur3 = new String[listDocteur3.size()][4];
					
					for(int i=0; i<listDocteur3.size(); i++)
					{
						for(int j=0; j<listDocteur3.get(i).size(); j++)
						{
						
							listeDocteur3[i][j] = listDocteur3.get(i).get(j).toString();
							
						}
					}
					
					/**a ajouter dans le table seulement si l'utilisateur a repsecter les conditions de remplissage*/
					tableDocteur3 = new JTable(listeDocteur3, columnNames);
					affiche.fp.panel_donnee.add(tableDocteur3.getTableHeader(),BorderLayout.NORTH);
				    affiche.fp.panel_donnee.add(new JScrollPane(tableDocteur3),BorderLayout.NORTH);
					
					
				}
				
			}
			
		}
		
	    affiche.fp.panel_donnee.revalidate();
	    affiche.fp.panel_donnee.repaint();
		
	}
	
	
	public void reporting() {
		
		ArrayList<Vector<String>> reportingList1 = new ArrayList<Vector<String>>();
		/**tests*/
		float ortho=0;
		float ane=0;
		float pneumo=0;
		float radio=0;
		float cardio=0;
		float trauma=0;
		float total=0;
		
		
		try {
			
			reportingList1 = connexion.remplirChampsRequete("SELECT specialite FROM docteur");
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		for(int i=0; i<reportingList1.size(); i++)
		{
			for(int j=0; j<reportingList1.get(i).size(); j++)
			{
				
				if(reportingList1.get(i).get(j).equals("Orthopediste")) ortho=ortho+1;
				
				if(reportingList1.get(i).get(j).equals("Cardiologue")) cardio=cardio+1;
					
				if(reportingList1.get(i).get(j).equals("Pneumologue")) pneumo=pneumo+1;
			
				if(reportingList1.get(i).get(j).equals("Radiologue")) radio=radio+1;
				
				if(reportingList1.get(i).get(j).equals("Anesthesiste")) ane=ane+1;
				
				if(reportingList1.get(i).get(j).equals("Traumatologue")) trauma=trauma+1;
									
			}
		}
		
		total=ortho+ane+pneumo+radio+cardio+trauma;
		affiche.fp.drawcamemberttest((ortho/total)*100,(cardio/total)*100,(trauma/total)*100,(ane/total)*100,(radio/total)*100,(pneumo/total)*100);
	
		
	}
	
	public void reporting3() {
		
		ArrayList<Vector<String>> reportingList1 = new ArrayList<Vector<String>>();
		/**tests*/
		float a=0;
        float b=0;
        float c=0;
        float d=0;
        float e=0;
        float f=0;
        float g=0;
        float h=0;
        float y=0;
        float z=0;
        float k=0;
        float l=0;
		
		
		try {
			
			reportingList1 = connexion.remplirChampsRequete("SELECT mutuelle FROM malade");
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		for(int i=0; i<reportingList1.size(); i++)
		{
			for(int j=0; j<reportingList1.get(i).size(); j++)
			{
				
				if(reportingList1.get(i).get(j).equals("LMDE")) a=a+1;
				
				if(reportingList1.get(i).get(j).equals("MNH")) b=b+1;
					
				if(reportingList1.get(i).get(j).equals("MAAF")) c=c+1;
			
				if(reportingList1.get(i).get(j).equals("MGEN")) d=d+1;
				
				if(reportingList1.get(i).get(j).equals("MMA")) e=e+1;
				
				if(reportingList1.get(i).get(j).equals("MNFTC")) f=f+1;
				
				if(reportingList1.get(i).get(j).equals("MNAM")) g=g+1;
				
				if(reportingList1.get(i).get(j).equals("MGSP")) h=h+1;
				
				if(reportingList1.get(i).get(j).equals("MAS")) y=y+1;
				
				if(reportingList1.get(i).get(j).equals("CNAMTS")) z=z+1;
				
				if(reportingList1.get(i).get(j).equals("CCVRP")) k=k+1;
				
				if(reportingList1.get(i).get(j).equals("AG2R")) l=l+1;
				
									
			}
		}
		float total=0;
		total=a+b+c+d+e+f+g+h+y+z+k+l;
		affiche.fp.drawcamembert((a/total)*100,(b/total)*100,(c/total)*100,(d/total)*100,(e/total)*100,(f/total)*100,(g/total)*100,(h/total)*100,(y/total)*100,(z/total)*100,(k/total)*100,(l/total)*100);
	
		
	}
	
	public void reporting4() {
		
		ArrayList<Vector<String>> reportingList1 = new ArrayList<Vector<String>>();
		/**tests*/
		float a=0;
        float b=0;
        float c=0;
       
        
		
		
		try {
			
			reportingList1 = connexion.remplirChampsRequete("SELECT code_service FROM infirmier");
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		for(int i=0; i<reportingList1.size(); i++)
		{
			for(int j=0; j<reportingList1.get(i).size(); j++)
			{
				
				if(reportingList1.get(i).get(j).equals("CHG")) a=a+1;
				
				if(reportingList1.get(i).get(j).equals("REA")) b=b+1;
					
				if(reportingList1.get(i).get(j).equals("OPH")) c=c+1;
			
				
				
									
			}
		}
		float total=0;
		total=a+b+c;
		affiche.fp.drawpieinfirmier((a/total)*100,(b/total)*100,(c/total)*100);
	
		
	}
	
	public void reporting2() {
		
		ArrayList<Vector<String>> reportingList2 = new ArrayList<Vector<String>>();
		//String salaire1=null,salaire2=null,salaire3=null;
		float avgjour=0;
		float avgnuit=0;
		float avgchg=0;
		float avgrea=0;
		float avgnull=0;
		
		try {
			
			reportingList2 = connexion.remplirChampsRequete("SELECT AVG(salaire) FROM infirmier WHERE rotation='JOUR'");
			System.out.println(reportingList2);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		avgjour=Float.parseFloat(reportingList2.get(0).get(0));
		System.out.println(avgjour);
		
		try {
			
			reportingList2 = connexion.remplirChampsRequete("SELECT AVG(salaire) FROM infirmier WHERE rotation='NUIT'");
			System.out.println(reportingList2);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		avgnuit=Float.parseFloat(reportingList2.get(0).get(0));
		System.out.println(avgnuit);
		
		try {
			
			reportingList2 = connexion.remplirChampsRequete("SELECT AVG(salaire) FROM infirmier WHERE code_service='CHG'");
			System.out.println(reportingList2);
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		
		avgchg=Float.parseFloat(reportingList2.get(0).get(0));
		System.out.println(avgchg);
		
		try {
			
			reportingList2 = connexion.remplirChampsRequete("SELECT AVG(salaire) FROM infirmier WHERE code_service='REA'");
			System.out.println(reportingList2);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		avgrea=Float.parseFloat(reportingList2.get(0).get(0));
		System.out.println(avgrea);
		
		try {
			
			reportingList2 = connexion.remplirChampsRequete("SELECT AVG(salaire) FROM infirmier WHERE code_service='OPH'");
			System.out.println(reportingList2);
		} catch (SQLException e1) {
			
			e1.printStackTrace();
		}
		
		avgnull=Float.parseFloat(reportingList2.get(0).get(0));
		System.out.println(avgnull);
		
		
		affiche.fp.drawbartest(avgjour,avgnuit,avgchg,avgrea,avgnull);
	}
	

}
