package controleur;


/*
 * 
 * Librairies importees
 */
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;



/**
 * 
 * Connexion a votre BDD locale ou a distance sur le serveur de l'ECE via le tunnel SSH
 * 
 * @author segado
 */
public class Connexion {
	
	
	

    /**
     * Attributs prives : connexion JDBC, statement, ordre requete et resultat
     * requete
     */
    private Connection conn;
    private Statement stmt;
    private ResultSet rset;
    private ResultSetMetaData rsetMeta;
    /**
     * ArrayList public pour les tables
     */
    public ArrayList<String> tables = new ArrayList<>();
    /**
     * ArrayList public pour les requetes de selection
     */
    public ArrayList<String> requetes = new ArrayList<>();
    /**
     * ArrayList public pour les requetes de MAJ
     */
    public ArrayList<String> requetesMaj = new ArrayList<>();

    /**
     * Constructeur avec 3 parametres : nom, login et password de la BDD locale
     *
     * @param nameDatabase Nom de la database
     * @param loginDatabase Identifiant de la database
     * @param passwordDatabase Mot de passe de la database
     * @throws java.sql.SQLException Exception SQL
     * @throws java.lang.ClassNotFoundException Exception Classe non trouvee
     */
    public Connexion(String nameDatabase, String loginDatabase, String passwordDatabase) throws SQLException, ClassNotFoundException {
  
        // chargement driver "com.mysql.jdbc.Driver"
        Class.forName("com.mysql.jdbc.Driver");

        // url de connexion "jdbc:mysql://localhost:3305/usernameECE"
        String urlDatabase = "jdbc:mysql://localhost:3306/" + nameDatabase;


        //creation d'une connexion JDBC a la base 
        conn = DriverManager.getConnection(urlDatabase, loginDatabase, passwordDatabase);

        // creation d'un ordre SQL (statement)
        stmt = conn.createStatement();
    }


    /**
     * Constructeur avec 4 parametres : username et password ECE, login et
     * password de la BDD a distance sur le serveur de l'ECE
     * @param usernameECE Identifiant ECE
     * @param passwordECE Mot de passe ECE
     * @param loginDatabase Identifiant database
     * @param passwordDatabase Mot de passe database
     * @throws java.sql.SQLException Exception SQL
     * @throws java.lang.ClassNotFoundException Exception Class Not Found
     */
    public Connexion(String usernameECE, String passwordECE, String loginDatabase, String passwordDatabase) throws SQLException, ClassNotFoundException {
        // chargement driver "com.mysql.jdbc.Driver"
        Class.forName("com.mysql.jdbc.Driver");

        // Connexion via le tunnel SSH avec le username et le password ECE
        SSHTunnel ssh = new SSHTunnel(usernameECE, passwordECE);

        if (ssh.connect()) {
            System.out.println("Connexion reussie");

            // url de connexion "jdbc:mysql://localhost:3305/usernameECE"
            String urlDatabase = "jdbc:mysql://localhost:3305/" + usernameECE;

            //creation d'une connexion JDBC a la base
            conn = DriverManager.getConnection(urlDatabase, loginDatabase, passwordDatabase);

            // creation d'un ordre SQL (statement)
            stmt = conn.createStatement();

        }
    }

    /**
     * Methode qui ajoute la table en parametre dans son ArrayList
     *
     * @param table Table en paramatre
     */
    public void ajouterTable(String table) {
        tables.add(table);
    }

    /**
     * Methode qui ajoute la requete de selection en parametre dans son
     * ArrayList
     *
     * @param requete Requete en parametre
     */
    public void ajouterRequete(String requete) {
        requetes.add(requete);
    }

    /**
     * Méthode qui ajoute la requete de MAJ en parametre dans son
     * ArrayList
     *
     * @param requete Requete en parametre
     */
    public void ajouterRequeteMaj(String requete) {
        requetesMaj.add(requete);
    }

    /**
     * Méthode qui retourne l'ArrayList des champs de la table en parametre
     *
     * @param table Table en parametre
     * @return ArrayList Resultat de requete
     * @throws java.sql.SQLException Exception SQL
     */
    public ArrayList remplirChampsTable(String table) throws SQLException {
        // recuperation de l'ordre de la requete
        rset = stmt.executeQuery("select * from " + table);

        // recuperation du resultat de l'ordre
        rsetMeta = rset.getMetaData();

        // calcul du nombre de colonnes du resultat
        int nbColonne = rsetMeta.getColumnCount();

        // creation d'une ArrayList de String
        ArrayList<String> liste;
        liste = new ArrayList<>();
        String champs = "";
        // Ajouter tous les champs du resultat dans l'ArrayList
        for (int i = 0; i < nbColonne; i++) {
            champs = champs + " " + rsetMeta.getColumnLabel(i + 1);
        }

        // ajouter un "\n" a la ligne des champs
        champs = champs + "\n";

        // ajouter les champs de la ligne dans l'ArrayList
        liste.add(champs);

        // Retourner l'ArrayList
        return liste;
    }

    /**
     * Methode qui retourne l'ArrayList des champs de la requete en parametre
     * @param requete Requete en parametre
     * @return ArrayList Resultat de requete
     * @throws java.sql.SQLException Exception SQL
     */
    public ArrayList<Vector<String>> remplirChampsRequete(String requete) throws SQLException {
        // recuperation de l'ordre de la requete
        rset = stmt.executeQuery(requete);

        // recuperation du resultat de l'ordre
        rsetMeta = rset.getMetaData();

        // calcul du nombre de colonnes du resultat
        int nbColonne = rsetMeta.getColumnCount();

        // creation d'une ArrayList de String
        ArrayList<Vector<String>> liste;
        liste = new ArrayList<Vector<String>>();

        // tant qu'il reste une ligne 
        while (rset.next()) {
            Vector<String> champs = new Vector<String>();
            champs.add(rset.getString(1)); // ajouter premier champ

            // Concatener les champs de la ligne separes par ,
            for (int i = 1; i < nbColonne; i++) {
                champs.add(rset.getString(i + 1));
            }

            // ajouter les champs de la ligne dans l'ArrayList
            liste.add(champs);
        }

        // Retourner l'ArrayList
        return liste;
    }

    /**
     * Méthode qui execute une requete de MAJ en parametre
     * @param requeteMaj Requete de mise a jour
     * @throws java.sql.SQLException Exception SQL
     */
    public void executeUpdate(String requeteMaj) throws SQLException {
        stmt.executeUpdate(requeteMaj);
    }
}
